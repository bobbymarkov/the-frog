## Welcome to our project.
This project was created for the Project Team Building Class at Saxion University during the 2022-2023 Academic Year.

To be able to run our program you need to run ThePilot application made by Saxion University. In case you don't have access to ThePilot application you see our 10 minute demo video which showcases the application:


 Then you can run The Pilot and GroundControl as well.If you want to close the program, you need to close TheFrog.exe manually.

Just a warning on the Ground Control's dashboards if you click on the map, automatic mode will be activated so be careful.

To see what every button does in both of the dashboard you can check out our Technical Document which is located in the
folder docs. There you can also find all of our documentation. Our CoC and DoD are in the "Scrum documentation.md" .

Our Pilot database is located here: the_frog_pilot/resources
Our Ground Control database is located here: ground_control/resources

Viktor Krastev - 526479
Jafar Alirahmi - 535566
Hoang Cao Bach Dang - 528382
Borislav Markov - 533390