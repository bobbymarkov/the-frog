package nl.saxion.ptbc.frog_pilot.AntiCollision;

import nl.saxion.ptbc.ObjectDisplay.Point2D;
import nl.saxion.ptbc.frog_pilot.ThePilot;

public class CollisionDetection {
    private final ThePilot pilot;
    private Point2D frogPosition;
    private double frogRotation;

    public CollisionDetection(ThePilot pilot) {
        this.pilot = pilot;
    }

    /**
     * Draws path in front (or behind) of the frog based on its angle and distance from the drive command, checking for collisions on that path.
     * @param inputCommand the drive command.
     * @return returns true if there is a collision in the path, false is there is no collision.
     */
    public boolean checkForCollision(String inputCommand) {
        double desiredDistance = desiredMovementDistance(inputCommand);

        if (desiredDistance < 0) {
            desiredDistance = changeForBackwards(desiredDistance);
        }

        int angle = getAngle(inputCommand);

        if (angle != 0) {
            fixAngle(angle);
        }

        double[] facingResultSplit = splitFacingResult(determineFacing());
        double mainAngle = facingResultSplit[0];
        double mainLeftOver = facingResultSplit[1];
        double otherLeftOver = facingResultSplit[2];

        desiredDistance = adjustForDiagonal(desiredDistance, Math.min(mainLeftOver, otherLeftOver));
        // The starting amount
        int positionCheckFrequencyBase = 5;
        // Takes the starting amount and multiplies by distance to get even points for all distances.
        int positionCheckFrequency = (int) Math.round(positionCheckFrequencyBase * desiredDistance);

        double[] angleRatios = assignRatiosForAngle(desiredDistance, mainLeftOver, otherLeftOver, mainAngle);

        double xPointIncremention = angleRatios[0] / positionCheckFrequency;
        double zPointIncremention = angleRatios[1] / positionCheckFrequency;

        double xFrogPath = frogPosition.getX();
        double zFrogPath = frogPosition.getZ();

        boolean hasCollision = false;

        for (int i = 0; i < positionCheckFrequency; i++) {

            if (hasCollision) {
                break;
            }
            xFrogPath += xPointIncremention;
            zFrogPath += zPointIncremention;

            for (Point2D point : pilot.getAllRadarPoints()) {

                double offSet = 2;
                if (checkPosition(xFrogPath, zFrogPath, point.getX(), point.getZ(), offSet)) {
                    hasCollision = true;
                    break;
                }
            }
        }

        return hasCollision;
    }

    /**
     * Inverts the distance the frog will travel and fixes the angle which will be checked, so it checks backwards.
     * @param desiredDistance the distance the frog will be moving from its current position.
     * @return the inverted desired distance.
     */
    private double changeForBackwards(double desiredDistance) {
        desiredDistance *= -1;

        if (frogRotation <= 180) {
            frogRotation += 180;
        } else {
            frogRotation -= 180;
        }

        return desiredDistance;
    }

    /**
     * Adds to the angle which will be checked for collisions, called if the angle of the drive command is different from 0.
     * @param angle the angle which needs to be added to the "forward" angle.
     */
    private void fixAngle(int angle) {
        if (angle > 0) {
            frogRotation += angle;
            if (frogRotation > 360) {
                frogRotation -= 360;
            }
        } else if (angle < 0) {
            frogRotation -= angle;
            if (frogRotation < 0) {
                frogRotation += 360;
            }
        }
    }

    /**
     * Checks position for collision.
     * @param posX the X of the wanted position. (frogs position)
     * @param posZ the Z of the wanted position. (frogs position)
     * @param colX the X of possible collision. (A collision point)
     * @param colZ the Z of possible collision. (A collision point)
     * @param offSet the range in which will be checked (radius not diameter)
     * @return True if posX and posZ, are within the range (offset) of colX,colZ (theres collision). False if they are not in range (No collision).
     */
    public boolean checkPosition(double posX, double posZ, double colX, double colZ, double offSet) {
        if (colX > posX - offSet && colX < posX + offSet && colZ > posZ - offSet && colZ < posZ + offSet) {
            System.err.println("Checking the position: " + colX + " for "
                    + (posX - offSet) + " to " + (posX + offSet) + " and " + colZ
                    + " for " + (posZ - offSet) + " to " + (posZ + offSet));
            System.err.println("Collision at " + colX + " | " + colZ);
            return true;
        }
        return false;
    }

    /**
     * Updates the info about the frog in the class.
     * @param position The position message received from the frog.
     */
    public void setLastFrogInfo(String position) {
        frogPosition = Point2D.messageToPoint2D(position);
        frogRotation = Double.parseDouble(Point2D.convertCommaToDot(position.split(" ")[5]));
    }

    /**
     * Split the DRIVE COMMAND received from The Frog into variables
     * @param inputCommand the drive command from The Frog
     * @return int array containing power as [0], angle as [1] and duration as [2]
     */
    private int[] convertInputToVariables(String inputCommand) {
        String[] inputSplit = inputCommand.split(" ");
        return new int[]{Integer.parseInt(inputSplit[2]), Integer.parseInt(inputSplit[3]), Integer.parseInt(inputSplit[4])};
    }
    /**
     * Converts drive command into string and returns only the angle value.
     * @param inputCommand the drive command.
     * @return the angle from the drive command.
     */
    private int getAngle(String inputCommand) {
        int[] inputToVariables = convertInputToVariables(inputCommand);
        return inputToVariables[1];
    }

    /**
     * Gives travel distance based on the input command.
     * @param inputCommand the drive command.
     * @return the travel distance.
     */
    private double desiredMovementDistance(String inputCommand) {
        double toReturn;
        int[] inputToVariables = convertInputToVariables(inputCommand);
        int power = inputToVariables[0]; // -2000 -1000 1000 2000
        //int angle = inputToVariables[1]; // -20 0 20
        int duration = inputToVariables[2]; // 1 2

//        if (angle == 0) {
//            toReturn = checkNoAngle(Math.abs(power), duration);
//        } else {
//            toReturn = checkWithAngle(Math.abs(power), duration);
//        }

        toReturn = getDistanceForPowerNDuration(Math.abs(power), duration);

        if (isReversed(power)) {
            toReturn *= -1;
        }
        return toReturn;
    }

    /**
     * Takes power and duration and returns travel distance for the frog.
     * @param power the power of the frogs movement
     * @param duration the duration of the frogs movement
     * @return returns the travel distance.
     */
    private double getDistanceForPowerNDuration(int power, int duration) {
        if (power == 1000 && duration == 1) {
            return 7; //3 (+4)
        } else if (power == 2000 && duration == 1) {
            return 11; //7 (+4)
        } else if (power == 1000 && duration == 2) {
            return 14; //10 (+4)
        } else if (power == 2000 && duration == 2) {
            return 29; //25 (+4)
        } else {
            throw new IllegalArgumentException("The values " + power + " and " + duration + " are not supported! How did you even reach this?");
        }
    }
//
//    private double checkNoAngle(int power, int duration) {
//        if (power == 1000 && duration == 1) {
//            return 7; //3 (+4)
//        } else if (power == 2000 && duration == 1) {
//            return 11; //7 (+4)
//        } else if (power == 1000 && duration == 2) {
//            return 14; //10 (+4)
//        } else if (power == 2000 && duration == 2) {
//            return 29; //25 (+4)
//        } else {
//            throw new IllegalArgumentException("The values " + power + " and " + duration + " are not supported! How did you even reach this?");
//        }
//    }

    /**
     * Checks if the frog is going backwards.
     * @param power the power of the frogs drive command.
     * @return true if backwards, false if forward.
     */
    private boolean isReversed(int power) {
        return power < 0;
    }

    /**
     * Determines rotation quartile based on the frogs rotation.
     * @return 0-1 = (+x,+z), 1-2 = (+x,-z), 2-3 = (-x,-z), 3-4 = (-x,+z)
     */
    private double determineFacing() {
        return frogRotation / 90;
    }

    /**
     * the rotation divided by 90 and splits it into array containing index part, fractal part and left over to next full number.
     * @param facingResult the result of dividing the rotation by 90;
     * @return (example number 1,25) [0] = 1, [1] = 0.25. [2] = 0.75 (1-0.25)
     */
    private double[] splitFacingResult(double facingResult) {
        long indexPart = (long) facingResult;
        double fracPart = facingResult - indexPart;
        double leftPart = 1 - fracPart;
        return new double[]{indexPart, fracPart, leftPart};
    }

    /**
     * Adjusts the ratio for which will be added to X and Z when drawing the points on the frogs path based on the angle / 90
     * @param desiredDistance the travel distance
     * @param mainRatio the bigger of 2 ratios for which X and Z will be moved
     * @param leftRatio leftover after taking the fractal part out of 1 splitFacingResult() [2]
     * @param mainAngle main left over after division splitFacingResult() [0]
     * @return
     */
    private double[] assignRatiosForAngle(double desiredDistance, double mainRatio, double leftRatio, double mainAngle) {
        double xRatio;
        double zRatio;

        if (mainAngle == 1) {
            xRatio = desiredDistance * leftRatio;
            zRatio = desiredDistance * mainRatio;

            zRatio *= -1;
        } else if (mainAngle == 2) {
            xRatio = desiredDistance * mainRatio;
            zRatio = desiredDistance * leftRatio;
            xRatio *= -1;
            zRatio *= -1;
        } else if (mainAngle == 3) {
            xRatio = desiredDistance * leftRatio;
            zRatio = desiredDistance * mainRatio;

            xRatio *= -1;
        } else {
            xRatio = desiredDistance * mainRatio;
            zRatio = desiredDistance * leftRatio;
        }

        return new double[]{xRatio, zRatio};
    }

    /**
     * Adjust desired distance for the rotation.
     * @param desiredDistance the travel distance.
     * @param otherLeftOver the leftover part of splitFacingResult() ([2])
     * @return returns the adjusted travel distance.
     */
    private double adjustForDiagonal(double desiredDistance, double otherLeftOver) {
        return desiredDistance * (otherLeftOver + 1);
    }
}
