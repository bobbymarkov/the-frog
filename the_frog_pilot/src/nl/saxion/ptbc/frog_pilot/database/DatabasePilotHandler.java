package nl.saxion.ptbc.frog_pilot.database;

import nl.saxion.ptbc.ObjectDisplay.Point2D;

import nl.saxion.ptbc.dbUtil.dbPilotConnection;

import nl.saxion.ptbc.shared.Mission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class DatabasePilotHandler {
    private static DatabasePilotHandlerListener listener;
    public static void setListener(DatabasePilotHandlerListener listener) {
        DatabasePilotHandler.listener = listener;
    }
    /**
     * Sends query to the database and returns mission id.
     * @return the mission id.
     */
    public static int getMissionIdFromDatabase() {
        try (Connection connection = dbPilotConnection.getConnection()) {
            int missionId = 0;
            String selectQuery = "SELECT MAX(mission_id) FROM Mission";
            try (PreparedStatement statement = connection.prepareStatement(selectQuery);
                 ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    missionId = resultSet.getInt(1);
                }
            }
            return missionId;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Saves collision points in the database.
     * @param radarPoints the collision points to be saved.
     * @param missionId the mission they are assigned to.
     */
    public static void saveCollisionPointsToDatabase(ArrayList<Point2D> radarPoints, int missionId) {
        try (Connection connection = dbPilotConnection.getConnection()) {
            // Clear existing radar points from the table
            String deleteQuery = "DELETE FROM RadarPoint";
            try (PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery)) {
                deleteStatement.executeUpdate();
            }
            String insertQuery = "INSERT INTO RadarPoint (radar_id, x, z, mission_id) VALUES (?, ?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                for (int i = 0; i < radarPoints.size(); i++) {
                    Point2D radarPoint = radarPoints.get(i);
                    statement.setInt(1, i + 1); // Set the radar_id as the index (1-based)
                    statement.setDouble(2, radarPoint.getX());
                    statement.setDouble(3, radarPoint.getZ());
                    statement.setInt(4, missionId);
                    statement.executeUpdate();
                }
            }

            if (listener != null) {
                listener.onMissionSaved("Collision points saved successfully!");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
}
