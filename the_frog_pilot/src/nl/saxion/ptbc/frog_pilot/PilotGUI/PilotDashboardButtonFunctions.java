package nl.saxion.ptbc.frog_pilot.PilotGUI;

import nl.saxion.ptbc.frog_pilot.ThePilot;
import nl.saxion.ptbc.frog_pilot.ThePilotPanel;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.Arrays;

import static nl.saxion.ptbc.shared.Shared.*;
import static nl.saxion.ptbc.shared.Shared.MAP_2D_BOTTOM_LEFT_X;

public class PilotDashboardButtonFunctions {

    private final ThePilot pilot;
    private final PilotDashboard dashboard;

    protected PilotDashboardButtonFunctions(ThePilot pilot, PilotDashboard dashboard) {
        this.pilot = pilot;
        this.dashboard = dashboard;
    }

    /**
     * Sends command to the frog based on the input of the text fields for the command, they are checked using checkInput()
     * @param powerTxtFld the text field providing the power
     * @param angleTxtFld the text field providing the angle
     * @param timeTxtFld the text field providing the duration
     */
    protected void manualCommandSetButton(JTextField powerTxtFld, JTextField angleTxtFld, JTextField timeTxtFld) {
        if (checkInput(powerTxtFld, new String[]{"1", "2"}) && checkInput(angleTxtFld, new String[]{"0", "20"}) && checkInput(timeTxtFld, new String[]{"1", "2"})) {
            int power = Integer.parseInt(powerTxtFld.getText()) * 1000;
            if (dashboard.isPowerReversed()) {
                power *= -1;
            }
            int angle = Integer.parseInt(angleTxtFld.getText());
            if (dashboard.isAngleReversed()) {
                angle *= -1;
            }

            if (dashboard.isHasStopped()) {
                dashboard.setHasStoppedToFalse();
                pilot.sendMessage("PILOT DRIVE " + power + " " + angle + " " + timeTxtFld.getText(), pilot);
            }
        }
    }

    /**
     * Checks whether a text field has correct input based on array with all possible correct inputs.
     * @param textField the text field which will be checked.
     * @param possibleCorrects the possible correct inputs.
     * @return true if fields text is in the possible Correct input list, false if it's not.
     */
    protected boolean checkInput(JTextField textField, String[] possibleCorrects) {
        if (Arrays.stream(possibleCorrects).filter(answer -> answer.equals(textField.getText())).toArray(String[]::new).length > 0) {
            textField.setForeground(Color.black);
            return true;
        }
        textField.setForeground(Color.red);
        return false;
    }


    /**
     * Function to reverse the frogs movement direction.
     * @param reversePowerBtn The button which will have this function.
     */
    protected void reversePowerButton(JButton reversePowerBtn) {
        if (dashboard.isPowerReversed()) {
            dashboard.setIsPowerReversed(false);
            reversePowerBtn.setText("Forward");
        } else {
            dashboard.setIsPowerReversed(true);
            reversePowerBtn.setText("Backwards");
        }
    }

    /**
     * Function to reverse the frogs turning direction.
     * @param reverseAngleBtn The button which will have this function.
     */
    protected void reverseAngleButton(JButton reverseAngleBtn) {
        if (dashboard.isAngleReversed()) {
            dashboard.setAngleReversed(false);
            reverseAngleBtn.setText("Right");
        } else {
            dashboard.setAngleReversed(true);
            reverseAngleBtn.setText("Left");
        }
    }

    /**
     * Function for button which updates the frogs radar
     * @param radarOnOffBtn the button which will have that function.
     */
    protected void setIsRadarOn(JButton radarOnOffBtn) {
        if (dashboard.isRadarOn()) {
            dashboard.setRadarOn(false);
            radarOnOffBtn.setText("Radar: Off");
            pilot.sendMessage("PILOT RADAR OFF", pilot);
        } else {
            dashboard.setRadarOn(true);
            radarOnOffBtn.setText("Radar: On");
            pilot.sendMessage("PILOT RADAR ON", pilot);
        }
    }

    /**
     * Changes the setting of the collision setting button.
     * @param ignoreCollisionBtn The button which will update the setting.
     */
    protected void ignoreCollisionButton(JButton ignoreCollisionBtn) {
        dashboard.incrementCollisionSetting();
        if (dashboard.getCollisionDetectionSetting() > 3) {
            dashboard.setCollisionDetectionSettingToStop();
        }
        updateStopButtonText(ignoreCollisionBtn);
    }


    /**
     * Function for the collision setting button's text to be updated.
     * @param ignoreCollisionBtn the collision detection button which will be updated.
     */
    protected void updateStopButtonText(JButton ignoreCollisionBtn) {
        if (dashboard.getCollisionDetectionSetting() == 1) {
            ignoreCollisionBtn.setText("Stop");
        } else if (dashboard.getCollisionDetectionSetting()  == 2) {
            ignoreCollisionBtn.setText("Skip Next");
        } else if (dashboard.getCollisionDetectionSetting()  == 3) {
            ignoreCollisionBtn.setText("No stop");
        }
    }

    /**
     * Gives action listener to text fields, which input will be checked by checkInput()
     * @param textField the text field will be checked
     * @param correctAnswers array of the possible correct inputs for the field
     */
    protected void addListenerToTextFields(JTextField textField, String[] correctAnswers) {
        textField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                checkInput(textField, correctAnswers);
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                checkInput(textField, correctAnswers);
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                checkInput(textField, correctAnswers);
            }
        });
    }

    /**
     * Function of the manual driving button, changes text, gives focus to the button, and locks other components
     * @param manualOnOffBtn the manual driving button
     * @param thePilotPanel the pilot panel from which is sent
     */
    protected void setSwitchManual(JButton manualOnOffBtn, ThePilotPanel thePilotPanel) {
        if (dashboard.isSwitchManual()) {
            dashboard.setSwitchManual(false);
            manualOnOffBtn.setText("Off");
            thePilotPanel.focusLost();
            unlockForAutomatic();
        } else {
            dashboard.setSwitchManual(true);
            manualOnOffBtn.setText("On");
            thePilotPanel.requestFocus();
            thePilotPanel.focusGained();
            lockForAutomatic();
        }
    }

    /**
     * turns off all components except manual driving mode button
     */
    protected void lockForAutomatic() {
        dashboard.powerTxtFld.setEnabled(false);
        dashboard.angleTxtFld.setEnabled(false);
        dashboard.timeTxtFld.setEnabled(false);
        dashboard.manualCommandSetBtn.setEnabled(false);
        dashboard.reversePowerBtn.setEnabled(false);
        dashboard.reverseAngleBtn.setEnabled(false);
        dashboard.exportImportMapBtn.setEnabled(false);
        dashboard.radarOnOffBtn.setEnabled(false);
        dashboard.showCollisionMapBtn.setEnabled(false);
        dashboard.ignoreCollisionBtn.setEnabled(false);
    }

    /**
     * turns on all components except manual driving mode button
     */
    protected void unlockForAutomatic() {
        dashboard.powerTxtFld.setEnabled(true);
        dashboard.angleTxtFld.setEnabled(true);
        dashboard.timeTxtFld.setEnabled(true);
        dashboard.manualCommandSetBtn.setEnabled(true);
        dashboard.reversePowerBtn.setEnabled(true);
        dashboard.reverseAngleBtn.setEnabled(true);
        dashboard.exportImportMapBtn.setEnabled(true);
        dashboard.radarOnOffBtn.setEnabled(true);
        dashboard.showCollisionMapBtn.setEnabled(true);
        dashboard.ignoreCollisionBtn.setEnabled(true);
    }

    /**
     * Converts position into UI position on the map
     * @param frogXRealPos the X of the position
     * @param frogZRealPos the Z of the position
     * @return the position corrected for the map
     */
    protected Point2D.Double calculatePosition(double frogXRealPos, double frogZRealPos) {
        double xPosOnUI = (frogZRealPos - MAP_2D_BOTTOM_LEFT_Z) * MAP_2D_UI_WIDTH_PILOT / (MAP_2D_TOP_RIGHT_Z - MAP_2D_BOTTOM_LEFT_Z); // x on UI is according to z on map
        double yPosOnUI = (frogXRealPos - MAP_2D_BOTTOM_LEFT_X) * MAP_2D_UI_HEIGHT_PILOT / (MAP_2D_TOP_RIGHT_X - MAP_2D_BOTTOM_LEFT_X); // y on UI is according to x on map

        return new Point2D.Double(xPosOnUI, yPosOnUI);
    }
}
