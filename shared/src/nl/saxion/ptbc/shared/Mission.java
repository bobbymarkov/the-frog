package nl.saxion.ptbc.shared;

import java.util.ArrayList;

public class Mission {
    private static int lastAssignedMissionId = 0;
    private int missionId;
    private String dateTime;
    private String name;
    private String description;
    private ArrayList<String> driveCommands = new ArrayList<>();

    /**
     * Increase missionId every time create a new mission to differentiate the ids of different missions
     */
    public Mission() {
        missionId = ++lastAssignedMissionId;
    }

    /**
     * Add drive commands to the mission's array list
     * @param command String drive command such as "PILOT DRIVE 1000 0 1"
     */
    public void addCommand(String command) {
        driveCommands.add(command);
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getDriveCommands() {
        return driveCommands;
    }

    public String getDescription() {
        return description;
    }

    public String getName() {
        return name;
    }

    public int getMissionId() {
        return missionId;
    }

    public void setMissionId(int missionId) {
        this.missionId = missionId;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
}
