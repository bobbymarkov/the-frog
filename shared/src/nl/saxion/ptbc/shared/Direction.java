package nl.saxion.ptbc.shared;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import static nl.saxion.ptbc.shared.Shared.STARTING_ANGLE;

public class Direction {
    private BufferedImage img;

    /**
     * Create an instance of the direction by adding image to the img variable inside
     * Then rotate image so that it is the same with the direction of the Frog
     * @param fileName link to the image
     */
    public Direction(String fileName) {
        img = null;
        InputStream is = Direction.class.getResourceAsStream("/" + fileName);
        try {
            img = ImageIO.read(is);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        rotateImg(STARTING_ANGLE);
    }

    /**
     * Draw the image
     * @param g Graphics g of paintComponent method
     * @param x x coordinate
     * @param y y coordinate
     * @param width width of image
     * @param height height of image
     */
    public void draw(Graphics g, int x, int y, int width, int height) {
        g.drawImage(img, x, y, width, height, null);
    }

    /**
     * Rotate the image according to the degree in the param.
     * @param degrees degree such as 45 degree,...
     */
    public void rotateImg(double degrees) {
        ImageIcon icon = new ImageIcon(img);
        BufferedImage blankCanvas = new BufferedImage(icon.getIconWidth(), icon.getIconHeight(), BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = (Graphics2D) blankCanvas.getGraphics().create();
        g2d.rotate(Math.toRadians(degrees), icon.getIconWidth() / 2, icon.getIconHeight() / 2);
        g2d.drawImage(img, 0, 0, null);
        img = blankCanvas;
    }
}
