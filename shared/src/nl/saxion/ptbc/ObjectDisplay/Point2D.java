package nl.saxion.ptbc.ObjectDisplay;

import java.util.ArrayList;

/**
 * Represents a 2D point with x and z coordinates.
 */
public class Point2D {
    private final double x;
    private final double z;

    /**
     * Constructs a Point2D object with the given x and z coordinates.
     *
     * @param x the x coordinate
     * @param z the z coordinate
     */
    public Point2D(double x, double z) {
        this.x = x;
        this.z = z;
    }

    /**
     * Returns the x coordinate of the point.
     *
     * @return the x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the z coordinate of the point.
     *
     * @return the z coordinate
     */
    public double getZ() {
        return z;
    }

    /**
     * Converts a message string in the format "FROG POSITION X Z Y W" to a Point2D object.
     *
     * @param message the message string to convert
     * @return a Point2D object representing the x and z coordinates extracted from the message
     */
    public static Point2D messageToPoint2D(String message) {
        String[] messageSplit = convertCommaToDot(message).split(" ");
        return new Point2D(Double.parseDouble(messageSplit[2]), Double.parseDouble(messageSplit[3]));
    }

    /**
     * Converts any commas in the input string to dots.
     *
     * @param input the input string to convert
     * @return the converted string with commas replaced by dots
     */
    public static String convertCommaToDot(String input) {
        return input.replace(',', '.');
    }

    /**
     * Converts a list of Point2D objects to a formatted string.
     *
     * @param pointList the list of Point2D objects to convert
     * @return a string representation of the Point2D objects in the list
     */
    public static String convertPointListToString(ArrayList<Point2D> pointList) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < pointList.size(); i++) {
            Point2D point = pointList.get(i);
            sb.append("RADAR POINT ")
                    .append(point.getX())
                    .append(" ")
                    .append(point.getZ());

            if (i < pointList.size() - 1) {
                sb.append(" ");
            }
        }

        return sb.toString();
    }
}
