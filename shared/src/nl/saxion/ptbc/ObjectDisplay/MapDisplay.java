package nl.saxion.ptbc.ObjectDisplay;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.*;

/**
 * A JPanel class that displays objects on a map.
 */
public class MapDisplay extends JPanel {
    private List<Point3D> coordinates;
    private final int windowWidth = 800;
    private int windowHeight = 500;

    private int offset = 10;
    private int x_radar = windowWidth / 2;
    private int z_radar = windowHeight - offset;
    private ArrayList<String> messages;

    /**
     * Constructs a MapDisplay object with the given messages.
     *
     * @param messages the messages containing the radar points
     */
    public MapDisplay(ArrayList<String> messages) {
        setPreferredSize(new Dimension(windowWidth, windowHeight));
        this.messages = messages;
    }

    /**
     * Sets the coordinates of the objects to be displayed on the map.
     *
     * @param coordinates the list of 3D coordinates of the objects
     */
    public void setCoordinates(List<Point3D> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);


        // Draw the map
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, getWidth(), getHeight());

        // Draw the objects
        if (coordinates != null) {
            for (Point3D point : coordinates) {
                Point2D point2D = convertTo2D(point);
                drawObject(g, point2D);
            }
        }
        ImageIcon imageIcon = new ImageIcon("shared/resources/TheFrog.png");
        Image image = imageIcon.getImage();
        g.drawImage(image, x_radar - imageIcon.getIconWidth()/2, z_radar - imageIcon.getIconHeight() + 10, null);
    }

    /**
     * Converts the given radar points represented as strings to a list of 3D points.
     *
     * @param radarPoints       the radar points represented as strings
     * @param firstPartOfMessage the first part of the message before the coordinates
     * @return a list of 3D points representing the radar points
     */
    public static ArrayList<Point3D> convertStringTo3D(ArrayList<String> radarPoints, String firstPartOfMessage) {
        ArrayList<Point3D> points3D = new ArrayList<>();
        for (String radarPoint : radarPoints) {

            // Extract the values after "FROG POINT"
            String[] values = radarPoint.split(firstPartOfMessage)[1].split(" ");
            List<Double> convertedValues = new ArrayList<>();
            for (String value : values) {
                double convertedValue = Double.parseDouble(value.replace(",", "."));
                convertedValues.add(convertedValue);
            }
            points3D.add(new Point3D(convertedValues.get(0), convertedValues.get(1), convertedValues.get(2)));
        }
        return points3D;
    }

    /**
     * Converts the given 3D point to a 2D point on the map display.
     *
     * @param object3D the 3D point to convert
     * @return the converted 2D point
     */
    private Point2D convertTo2D(Point3D object3D) {
        double x_object = object3D.getX() * 5;
        double z_object = object3D.getZ() * 5;

        x_object += x_radar;
        z_object = z_radar - z_object;

        return new Point2D(x_object, z_object);
    }

    /**
     * Draws the object on the map display using the given graphics object.
     *
     * @param g     the graphics object to draw with
     * @param point the 2D point representing the object's position
     */
    private void drawObject(Graphics g, Point2D point) {
        int radius = 5;
        int x = (int) point.getX();
        int z = (int) point.getZ();

        g.setColor(Color.RED);
        g.fillOval(x - radius, z - radius, 2 * radius, 2 * radius);
    }

    /**
     * Entry point of the application.
     */
    public void main() {
        ArrayList<Point3D> coordinates = convertStringTo3D(messages, "FROG POINT ");

        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        setCoordinates(coordinates);

        frame.add(this);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }
}
