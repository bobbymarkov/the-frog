package nl.saxion.ptbc.ObjectDisplay;

/**
 * Represents a 3D point with x, y, and z coordinates.
 */
public class Point3D {
    private double x;
    private double z;
    private double y;

    /**
     * Constructs a Point3D object with the given x, z, and y coordinates.
     *
     * @param x the x coordinate
     * @param z the z coordinate
     * @param y the y coordinate
     */
    public Point3D(double x, double z, double y) {
        this.x = x;
        this.z = z;
        this.y = y;
    }

    /**
     * Returns the x coordinate of the point.
     *
     * @return the x coordinate
     */
    public double getX() {
        return x;
    }

    /**
     * Returns the y coordinate of the point.
     *
     * @return the y coordinate
     */
    public double getY() {
        return y;
    }

    /**
     * Returns the z coordinate of the point.
     *
     * @return the z coordinate
     */
    public double getZ() {
        return z;
    }
}
