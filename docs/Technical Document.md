# TECHNICAL DOCUMENT
## Wireframes
### Pilot UI wireframe
![](.Technical Document_images/611d572d.png)
Manual button: Clicking on it will disable other button and text field and change
focus to the keyboard inputs.

Power text field: Only allow 1 or 2.

Angle text field: Only allow 0 or 20.

Time text field: Only allow 1 or 2.

Forward/ Backward: Switch between going forward and backward

Right/ Left: Switch between going right or left

Stop/ Skip Next/ No stop: Stop means it will stop the Frog if 
there's a collision ahead. Skip Next means it will skip the next
detected obstacles and allow the Frog to move. No stop means the
program will not stop the Frog from crashing with obstacles.

Go button: By pressing this button, the value in the three mentioned text fields
will be used to form a PILOT DRIVE command. For the Power text field, 1 or 2 will 
become 1000 or 2000. The Forward/ Backward and Right/ Left button will indicate 
its direction. Example: PILOT DRIVE 1000 20 2 can be done by input 1 20 2 in
turn to the Power, Angle and Time text fields.

Map: this is the zone to display the 2D map.

Save collision-point map: Save the collision-point map to the database.

Radar on/off: Turn on/ off the radar.

Collision Map: Open a new window to display the collision points detected and their
distance compared to the Frog.

### Ground control UI wireframe
![](.Technical Document_images/aac73e61.png)
Map: The zone to display the 2D map. By clicking on the map, the Frog will move
auto automatically to that point.

X text field: Only accept value from -78,75 to 421,29.

Z text field: Only accept value from -145,89 to 354,13.

GO button: Take two values from the X and Z text field to perform the automatic
movement function.

Add mission button: Enabling the "Name", "Description", "List of commands" and all
elements below it, ... As well as initialize a new mission variable.
    
    Name: Type in a name for the mission.
    Description: Type in the description of the button
    List of commands (JTextArea): Displaying a list of commands. Uneditable.
    Power, Angle, Duration: Same with the text fields from Pilot.
    Add command (Button): Take all the values from the text fields to form a drive command and added it to the mission
(display on the list of commands also)
    Send mission to Pilot (Button): Send the list of drive commands to the Pilot for the Frog to execute.

Save current mission button: Save the current mission to the database.

Import mission button: Import a mission (csv file) from your directory
and execute it.

Export mission button: Export the current mission to a csv file.

Request detected obstacles from Frog button: Request the radar points
from the Pilot.

Save collision-point map button: Save the Collision-point map to the 
database.

Import collision-point map button: Import a collision-point map (csv file)
from your directory and display them on the 2D map

Export collision-point map button: Export the current radar points to
a Csv file.

Action log (JText Area): Display all the action done by the Frog, 
Pilot and Ground control.


## Database design
![](.Technical Document_images/img.png)

####  Introduction
"THE PILOT" and "THE GROUND CONTROL" systems share a similar database design and will be connected through the Sasa API.
The database design includes the following tables: Mission, Map, RadarData, and DriveCommand. 
The Mission table is related to the Map table through the mission_id column. Each mission can have multiple maps 
associated with it. The Mission table is related to the RadarPoint table through the mission_id column. Each mission can
have multiple radar data entries. The Mission table is related to the DriveCommand table through the mission_id column.
Each mission can have multiple drive commands. These tables capture essential information about missions, maps,
radar points, and drive commands. The relationships between these tables allow for efficient querying and retrieval
of data. In the future, the two systems will be connected through the Sasa API, enabling seamless communication and
data exchange.

#### Mission Table
The Mission table stores information about a specific mission. It includes the following columns:
-	mission_id (Primary Key): An integer value that uniquely identifies a mission.
-	mission_name (Text, Not Null): The name of the mission.
-	description (Text): A description of the mission.
-	date_time (Text): The date and time of the mission.
#### Map Table
The Map table contains information about the maps associated with each mission. It includes the following columns:
-	map_id (Integer, Not Null): An integer value that uniquely identifies a map.
-	date_time (Text): The date and time of the map.
-	mission_id (Foreign Key): A reference to the mission the map belongs to.
#### RadarPoints Table
The RadarPoints table stores radar data collected during a mission. It includes the following columns:
-	radar_id (Primary Key): An integer value that uniquely identifies radar data.
-	mission_id (Foreign Key, Not Null): A reference to the mission the radar data belongs to.
-	x (Real, Not Null): The x-coordinate value of the radar data.
-	y (Real): The y-coordinate value of the radar data.
-	z (Real, Not Null): The z-coordinate value of the radar data.
#### DriveCommand Table
The DriveCommand table contains information about the drive commands issued during a mission. It includes the following
columns:
-	dc_id (Primary Key): An integer value that uniquely identifies a drive command.
-	mission_id (Foreign Key, Not Null): A reference to the mission the drive command belongs to.
-	message (Text): The command message.


## Relative information about framework choices
Our group use Java.Swing as our main framework as Java.Swing is easy to learn and understand. Most of what
is required to use this framework is object-oriented programming which is what we have just learned in the
third quarter, so using it will improve our OOP skill tremendously.

Also, Swing has been used for many years and has a large user base, which means there is extensive documentation, 
tutorials, and community support available. Thus, it is easier for us to do research about this framework to 
understand how it works before starting the project.

### Classes

To see all information about the classes create a java docs document by
pressing Tools => Generate JavaDocs
