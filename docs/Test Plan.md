# Test Plan

## Test Case 1
Name: A driver can drive the Frog in manual mode.

Requirement: RFU-01

1. Try to write a number in the first text field for the power.
2. Try to write a number in the second text field for the angle.
3. Try to write a number in the third text field for the duration.
4. Press the button "Drive".

## Test Case 2
Name: A driver can view the received radar-points in a 2D-graph

Requirement: RFU-02
1. Run ThePilot.
2. On the UI of ThePilot click "Collision map"
3. A map with collision points appears.
4. Move the frog and press the button again.
5. A different map appears.

## Test Case 3
Name: A driver can view the position and the direction of the Frog and the driven
route on a 2D-map (a given image of the part of the Moon where the Frog
is)

1. Run the Frog in manual mode in the Pilot UI.
2. Move the Frog.
3. Observe if the driven route appears.

Requirement: RFU-03


## Test Case 4
Name: The Frog can send a mission log (a list of coordinates) to Ground Control.

1. Drives the Frog manually.
2. Check if the 2D map in the Ground control would update its path every time the Frog stops.
Requirement: RFS-01


## Test Case 5
Name: The Frog can send a map of the world (coordinates of obstacles seen so far)
to Ground Control on request.

Requirement: RFS-02

1. Press "Request detected obstacles from Frog" button.
2. Wait for map to be updated.

## Test Case 6
Name: The Frog can drive automatically to a given (preferably received from
Ground Control or by pointing that location on the 2D-map) location, driving
around obstacles.

Requirement: RFS-03

1. Open the ground control
2. Press on the map
3. The frog should turn towards the desired location
4. The frog should stop in front of obstacles if there are any
5. The frog should turn and attempt to go around the obstacle.


## Test Case 7
Name: The Frog can drive a pre-programmed mission (a list of drive statements),
preferably received from Ground Control.
Requirement: RFS-04

1. Left name blank
2. Left description blank
3. Add no drive commands to mission
4. Power, Angle, Duration incorrect format.
5. Add 1 commands to mission
6. Add multiple commands to mission

## Test Case 8
Name: The Frog prevents collisions when driving (in automatic as well as in manual
mode).
Requirement: RFS-05

1. Send a command when the frog is facing and close a wall
2. Send a command when the frog is facing the opposite way of a wall, and try moving backwards

## Test Case 9
Name: The operator see/ follow the position and the direction of the Frog on a
2D-map (a given image of the part of the Moon where the Frog is),
together with the driven route.
Requirement: RGU-01

1. Run the Frog in manual mode in the Pilot UI.
2. Move the Frog.
3. Observe if the arrow and the driven route appears.

## Test Case 10
Name: The operator can request a map (a list of detected obstacles) from the
Frog and see the result on his 2D-map (a given image of the part of the
Moon where the Frog is).
Requirement: RGU-02

1. Press "Request detected obstacles from Frog" button.
2. Wait for map to be updated.

## Test Case 11
Name: The operator can request the Frog to drive to a specific location,
preferably by pointing the location on the map.

Requirement: RGU-03

1. Open the ground control
2. Press on the map
3. The frog should turn towards the desired location
4. The frog should stop in front of obstacles if there are any
5. The frog should turn and attempt to go around the obstacle.

## Test Case 12
Name: The operator can send a mission log (a sequence of drive commands) to
de Frog for it to drive.

Requirement: RGU-04

1. Left name blank
2. Left description blank
3. Add no drive commands to mission
4. Power, Angle, Duration incorrect format.
5. Add 1 commands to mission
6. Add multiple commands to mission

## Test Case 13
Name: A mission can be exported to and imported from a CSV-file
Requirement: RGS-01

For import:
1. Open the ground control program and navigate to the "Missions" tab.
2. Ensure that a mission CSV file is available for importing.
3. Select the "Import mission" button.
4. Choose the CSV file for import.
5. Verify that the import process completes without errors.
6. Check that the commands from the imported mission are successfully sent to the Pilot.
7. Confirm that The Frog starts driving according to the imported mission.

For export:
1. Open the ground control program and ensure that it is in the desired state with missions present.
2. Press the "Export missions" button.
3. Verify that a CSV file is created and saved in the expected location.
4. Inspect the CSV file and ensure that it contains the "Drive" commands in the correct order, matching the order they were sent.
5. Check the terminal log to confirm that it displays the export process correctly.

## Test Case 14
Name: A mission can be saved in the database
Requirement: RGS-02

1. Ensure that the ground control program is open and a mission is present.
2. Press the "Save current Mission" button.
3. Verify that the mission is successfully saved without any errors or exceptions.
4. Check the list of missions on the database to ensure that the saved mission is included.

## Test Case 15
Name: A map of collision-points can be exported to and imported from a CSV-file
Requirement: RGS-03

For import:
1. Have a csv file with points that you know where they should appear on the map.
2. Press on the "Import Collision-point Map" button
3. Choose the csv file that was created.
4. Confirm choice.
5. Check the Ground Control's map on the dashboard if the new points have appeared.
6. Do the same for The Pilot's map on the dashboard

For export:
1. Request Radar Points.
2. Press "Export Collision Point Map" button.
3. Open the newly created file.

## Test Case 16
Name: A map of collision-points can be saved in the database
Requirement: RGS-04 

1. Set up a test environment with the necessary dependencies, including the Ground Control Dashboard and a database system supporting SQLite.
2. Launch the Ground Control Dashboard and establish a connection with the system.
3. Simulate or receive radar points in the Ground Control Dashboard.
4. Ensure that the "Save Collision Point Map" button is visible and clickable.
5. Click the "Save Collision Point Map" button.
6. Verify that the radar points are successfully saved to the "RadarPoint" table in the database.

## Test Case 17
Name: Using Automatic Mode without having to avoid an obstacle.
Requirement: RGU-03 

1. Run ThePilot
2. Run GroundControl
3. On the Ground Control's dashboard select a point on the map where The Frog doesn't have to avoid any obstacles.
4. Wait for it to reach its destination.

# Test Traceability Matrix
![](.Test Plan_images/img.png)
