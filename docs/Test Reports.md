# Test Report
## Sprint 1
### Test Case 5, 10, US-20 - Success
Step 1,2,3
![img_6.png](.Test Reports_images/img_6.png)
Step 4,5
![img_7.png](.Test Reports_images/img_7.png)
### US-01-BI1 -Show the frog's position on the moon-map-Success
![img_8.png](.Test Reports_images/7df01615.png)
### Test Case 9, US-21 - send messages from The Pilot to the Ground Control-Success
![img_21](.Test Reports_images/04baa6e3.png)
### US-22 - Success
![img_9.png](.Test Reports_images/1756cb40.png)

### Test Case 1, US-23 Success
Step: 1.
![](.Test Reports_images/c22d2b22.png)
Step 2.
![](.Test Reports_images/37bbe9ec.png)
Step 3.
![](.Test Reports_images/fa652eef.png)
Step 4.
![](.Test Reports_images/695ee5de.png)

### Tes Case 2, US-24 Success
Step: 1.
![](.Test Reports_images/d74c608a.png)
Step: 2.
![](.Test Reports_images/5fb45946.png)
Step: 3. ,4.
![](.Test Reports_images/3b596176.png)

### US-25 Success
Step: 1.
![](.Test Reports_images/1e62ab78.png)
Step: 2.
![](.Test Reports_images/10f5d1a0.png)

### US-12 Success
Step 1: Turn on manual mode
![US_12_Step1_1.png](.Test Reports_images/42f33562.png)
![US_12_Step1_2.png](.Test Reports_images/c4113535.png)
Step 2: Move forward
![US_12_Step2.png](.Test Reports_images/dd5b39f5.png)
Step 3: Move forward/ backward to the left.
![US_12_Step3_1.png](.Test Reports_images/5265595b.png)
![US_12_Step3_2.png](.Test Reports_images/c50f27d0.png)
Step 4: Move backward
![US_12_Step4.png](.Test Reports_images/b704822f.png)
Step 5: Move forward/ backward to the right.
![US_12_Step5_1.png](.Test Reports_images/bebb4ee8.png)
![US_12_Step5_2.png](.Test Reports_images/50b51a20.png)

### US-25 Success
Step 1: Try moving
![US_25_Step1.png](.Test Reports_images/c213de5f.png)
Step 2: Click out of the window to see if the Frog stops.
![US_25_Step2.png](.Test Reports_images/7ac0c35e.png)

### US-22 BI-1 Success
![](.Test Reports_images/5c01c6dc.png)
![](.Test Reports_images/ae3cfd4d.png)

### Test Case 3, 4, 9, US-01 BI_2 and US-03 and US-21 (Frog's current location, facing direction and driven route on 2D map)
Step 1: Move the frog
Step 2: observe changes in current location, direction and driven route
![](.Test Reports_images/ee0d77aa.png)
![](.Test Reports_images/07de9dc3.png)
![](.Test Reports_images/a3c349e9.png)

## Sprint 2
### US-12 BI-1 Success
For manual movement:
![](.Test Reports_images/d486818a.png)
![](.Test Reports_images/ae67ecea.png)
![](.Test Reports_images/8459e964.png)

Through send a command:
![](.Test Reports_images/6c5bfd67.png)
![](.Test Reports_images/bde49841.png)
![](.Test Reports_images/fa1a333d.png)

### US-25 BI-1 Success
![](.Test Reports_images/ccc1030a.png)
![](.Test Reports_images/4ac90f88.png)
![](.Test Reports_images/db39cc8e.png)
![](.Test Reports_images/aac6fc97.png)
![](.Test Reports_images/c424d697.png)
![](.Test Reports_images/e2a3c133.png)

### Test Case 8, US-16 Success

![](.Test Reports_images/ba07a0c6.png)
![](.Test Reports_images/c6a9cb03.png)

### US-16 BI-1 Success
![](.Test Reports_images/b5c0deaa.png)

### US-13 Success
Test 1. Manual mode
![img.png](.Test Reports_images/img.png)
Test 2. Manual command input
![img.png](.Test Reports_images/US-13 test 2.png)

### Test Case 7, 12, US-05 Success
Left name blank
![](.Test Reports_images/dcfdf54d.png)
Left description blank
![](.Test Reports_images/b1df385e.png)
Add no drive commands to mission
![](.Test Reports_images/e51356dd.png)
Power, Angle, Duration incorrect format
![](.Test Reports_images/b8ecd06d.png)
![](.Test Reports_images/3572bc50.png)
![](.Test Reports_images/27f9cfc4.png)
Add 1 commands to mission
![](.Test Reports_images/9fa842cf.png)
Add multiple commands to mission
![](.Test Reports_images/66ba5f56.png)
Send mission to the frog
![](.Test Reports_images/23b5c2b0.png)
### Test Case 14, US-08 Success 
Save mission information into Ground Control's database
![](.Test Reports_images/img_1.png)

### US-20 Success (Borislav - 19.06.2023 08:56)
Button not pressed yet.
![img.png](.Test Reports_images/US-20 1.png)
After button being pressed.
![img_1.png](.Test Reports_images/US-20 2.png)

### Test Case 15, US-09 Success (Borislav - 19.06.2023 09:02)
After button has been pressed.
![img_1.png](.Test Reports_images/US-09 1.png)
The CSV file that was created.
![img_2.png](.Test Reports_images/US-09 2.png)
### Test Case 13, US-06 Success
The missions were exported to a CSV file
![](.Test Reports_images/img_2.png)
### US-26 Success 
Save the collision points maps into the Pilot's database
![](.Test Reports_images/img_4.png)
### US -11 Success 
Save the collision points maps into the GroundControl's database
![](.Test Reports_images/img_3.png)
### Test Case 13, US -07 Success
Before importing mission 
![](.Test Reports_images/img_5.png)
After importing mission 
![](.Test Reports_images/img_8.png)

### Test Case 15, US-10 Success (Borislav - 22.06.2023 12:26)
The csv file used
![US-10 1.png](.Test Reports_images/US-10 1.png)

The Pilot's map next to the Ground Control's map before importing the csv file
![img_1.png](.Test Reports_images/US-10 2.png)

Selecting the file
![img_2.png](.Test Reports_images/US-10 3.png)
    
After the button has been pressed. There is a new line which is visible after importing the file.
![img_3.png](.Test Reports_images/US-10 4.png)

### Test case 6, 16, 11, US-14
![](.Test Reports_images/d5dfffad.png)
Pressing on map:
![](.Test Reports_images/4a7b8a19.png)

### Test Case 17 (Borislav - 24.06.2023 21:48)
Before clicking on the map.
![img.png](.Test%20Reports_images/TestCase17-1.png)

After clicking on the map. The Frog has started driving.
![img_1.png](.Test%20Reports_images/TestCase17-2.png)

In the process 
![img_2.png](.Test%20Reports_images/TestCase17-3.png)

What ThePilot sees
![img_3.png](.Test%20Reports_images/TestCase17-4.png)

The Frog has reached its destination
![img_4.png](.Test%20Reports_images/TestCase17-5.png)