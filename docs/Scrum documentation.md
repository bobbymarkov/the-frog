# SCRUM DOCUMENTATION

### Introduction

Welcome to our Scrum Board documentation. In this document, we will provide an overview of our project management
process using GitLab's Scrum Board. Our Scrum Board is a tool that helps us organize and prioritize our work, as well
as monitor our progress and collaborate effectively. It provides a visual representation of our project's progress,
with each Sprint represented as a separate column.

Below is a screenshot with all the member's names and profile picture
![img_8.png](img_8.png)


The first components we will cover are our Definition of Done and Code of Conduct. Our Definition of Done outlines the
criteria that each Task must meet before it can be considered complete. This ensures that all work is of a
consistently high quality and meets our project requirements. Our Code of Conduct outlines the expectations we have
for team members and the consequences for not meeting them. This helps us maintain a positive and respectful team
environment, which is crucial for the success of our project.

Throughout the document, we will also cover our Retrospectives process. We conduct a Retrospective at the end of each
Sprint, where we reflect on our progress and identify areas for improvement. This helps us continuously improve our
project management process and ensure that we are meeting our project goals.

In total, we have 3 Sprints for our project. For each Sprint, we will include a list of items in the Backlog, with
accompanying descriptions and screenshots to provide context. We will also outline our process for creating and
completing User Stories/Tasks, as well as our criteria for meeting the Definition of Done.

Our roles are:
Product Owner: Borislav
Developer: Jafar
Developer: Bach
Developer: Viktor

Each Sprint has a Scrum Master.
For Sprint 1 the Scrum Master will be Borislav.
For Sprint 2 the Scrum Master will be Jafar.
For Sprint 3 the Scrum Master for the first week will be Viktor and for the second it will be Bach.

By the end of this document, you will have a comprehensive understanding of our project management process and how we
use GitLab's Scrum Board to streamline our work and ensure the success of our project.

### Code of Conduct

Our team is committed to creating a productive and positive working environment that fosters teamwork and
collaboration. In order to achieve this goal, we have developed a Code of Conduct that outlines our expectations for
team members.

We expect all team members to put in their fair share of work and complete their assigned tasks on time.
We understand that unexpected events may arise, but we expect team members to communicate any issues they
are experiencing as soon as possible. Effective communication is crucial for our team's success. We expect all
team members to communicate clearly and promptly, whether it be through Discord, email, or in-person conversations.
This includes informing the team if there are any personal or external issues that may impact their ability to
complete their assigned tasks.

Respect is another key aspect of our team's culture. We expect all team members to treat each other with respect
and consideration. This means being mindful of others' time, opinions, and feelings, and refraining from any behavior
that could be considered bullying, harassment, or discrimination.

Accountability is crucial to the success of our project. We expect all team members to fulfill their responsibilities
and to communicate clearly and honestly with the rest of the team. If a team member fails to complete their work
between stand-up meetings, an explanation is required. In this case, if the explanation is "I didn't have the
motivation," the team will accept it, but the member will be expected to catch up and spend double the time to
make up for the missed work.

If a team member fails to complete their work for a second time and the explanation is simply "I was lazy," without
a legitimate reason such as illness, then a strike will be issued. Craig will be notified of the strike, and the
team member will be expected to catch up and spend double the time to make up for the missed work. We believe that
this approach will encourage all team members to take their responsibilities seriously and to work together to
achieve our goals.

We have established a meeting schedule to ensure regular communication and collaboration within our team. We have
planned to hold three meetings each week, typically scheduled for Monday, Wednesday, and Friday. This means that we
would have enough time between meetings for us to make progress. These meetings will serve as
valuable opportunities for us to discuss project progress, address any challenges, and plan next steps. Of course, due
to unexpected circumstances sometimes our meetings might not happen on the scheduled days, but then they will just be
rescheduled.

Sometimes meetings will be held in Discord. This will be our main platform for communication. Discord
offers features such as screen sharing and voice chat, enabling us to have interactive and productive meetings. It
provides flexibility in terms of scheduling the meetings at a suitable time for all team members, regardless of their
physical location.

Finally, continuous improvement is crucial for our team's success. We are committed to continuously improving
our processes and our team members. This means actively seeking feedback and constructive criticism, and being
open to making changes when necessary.

By agreeing to this Code of Conduct, we believe that our team can accomplish our project goals.

### Definition of Done

The Definition of Done is an important aspect of our project. It defines the criteria that must be met for each
user story or task to be considered complete. Our Definition of Done includes the following criteria:

1. The code is tested and all tests have been successful.
2. The code has been pushed to GitLab and other team members have been notified.
3. Questions and suggestions regarding the code have been addressed and incorporated into the code.
4. The code has javaDoc comments, if a method is not that important(getters, setters or one line methods in general)
it's not necessary.

# Sprint 1 - Scrum Master - Borislav

Our Goal for this sprint was to set up the structure for our Scrum Documentation, Create the UI for The Pilot and The
Ground Control, Manual Mode, Establish the connection between The Frog, The Pilot and The Ground Control, Display
the lastly scanned radar points on a separate window and try to figure out what JDBC is.

## Backlog

![img(1).png](Sprint 1 Images/img(1).png)
![img.png](Sprint 1 Images/img.png)
![img_1.png](Sprint 1 Images/img_1.png)
![img_2.png](Sprint 1 Images/img_2.png)

## Meetings

### Meeting 1 - 14.05.2023 13:00

In this meeting the product backlog was created.

### Meeting 2 - 15.05.2023 13:03

After our meeting with Craig in this meeting we discussed which items should be put in Sprint 1.
The result can be seen in the Backlog.

### Meeting 3 - 22.05.2023 13:31

![img_3.png](Sprint 1 Images/img_3.png)
![img_4.png](Sprint 1 Images/img_4.png)
Due to the holidays there was a big gap between our meetings. Our progress was discussed and everybody were almost
ready with some of their tasks.

### Meeting 4 - 24.05.2023 20:00

![img_5.png](Sprint 1 Images/img_5.png)
This meeting was made via discord. A lot of progress was made between the two meetings. The progress was discussed
and future work was also discussed.

### Meeting 5 - 28.05.2023 12:00

![img_9.png](Sprint 1 Images/img_9.png)
This meeting was made via discord on Sunday because it was a more suitable time for our members. The progress was
discussed and also a structure was made for the Test Report document.
All the items with their User Story can be seen in the [Appendix](Scrum%20documentation.md#Appendix)

## End Of Sprint
Board at the End Of Sprint

![endOFSprint.png](Sprint 1 Images/img_9.png)

![img.png](Sprint%203%20Images/Sprint1_Charts.png)

Overall, the Sprint went well. We were able to complete our tasks except one, which was Figuring out JDBC. We just
decided that at the moment other issues were more important. We will definitely complete the task later on. Overall
the Sprint was successful and everybody contributed to the project. We met our goal, and now we are looking forwards to
Sprint 2.

## Retrospectives

### Borislav

During Sprint 1, we worked well. We were a little confused with how to use GitLab and how to structure our commits,
but eventually we understood, and I think that this something that we can improve on. I was responsible for writing our
Code Of Conduct, Definition of Done and overall create a structure for our Sprint Documentation. I was also able to make
the most recently scanned radar points to be shown when you click on a button on The Pilot’s dashboard. Overall, it was
a good sprint. I think that we are working well as a team, and we are going to be able to achieve a lot in the other
sprints.

### Viktor

During sprint 1, The group was doing well, everyone was doing their tasks, genuinely happy with how it went. I started
learning how gitlab works, how and much to commit. Made the pilot UI, and connected it with the frog. Added the receive
and send methods. Also made it possible to switch between manual mode where you control the frog through keyboard button
inputs (W,A,S,D), and manually through a command. Not to confuse, I did not create the (W,A,S,D) movement using
keyboard, I only made the switch between. Overall I think the average group member did more than me, so I will try to do
more the next sprints.

### Bach

During Sprint 1s, several aspects went well. Firstly, the code structure was successfully set up, progressing from the
initial pilot stage to the creation of the Pilot Dashboard (JFrame) and Pilot Panel (JPanel). Additionally, I also
completed the Ground Control User Interface (UI) and the Manual Control mode.

However, there were a few areas that did not go as smoothly. One such area was the lack of detailed descriptions in the
commit messages.

To improve the development process, it is recommended to ensure that commit messages are more descriptive, providing
clear and concise explanations of the changes made. Additionally, linking commits to corresponding user stories can
enhance traceability and help maintain a structured and organized development workflow.

### Jafar

In Sprint 1, there were some things that went well. I was responsible for communication between The Pilot and Ground
Control, and I successfully fulfilled that role. I was able to relay information and show the position of the Frog in
the 2D Moon-map. Additionally, I took the initiative to learn more about GitLab and understand the structure of our
codebase. This helped me contribute to the project and collaborate with the team. However, there were some challenges
that arose during Sprint 1. One area where things didn't go well was with the JDBC API. Due to time constraints, I was
unable to complete the JDBC task. This is something I acknowledge as an area for improvement and I will strive to finish
it in the next sprint. Another issue we faced as a team was related to GitLab and our code comments. We encountered
difficulties in properly utilizing GitLab for code management and struggled with leaving meaningful comments in our
commits. This led to some confusion and inefficiencies. It's important for us to address these challenges and find ways
to improve our GitLab workflow and communication around code commits in the upcoming sprint.

# Sprint 2 - Scrum Master - Jafar
## Goal
Our Goal for this sprint was to display direction and path has travelled by The Frog on 2DMap, Make restriction for the
movement,request a map of the world with the observed obstacles from The Pilot, figure out using JDBC and save data into
database and CSV-file, the frog to be able to not crash into
obstacles by seeing them and avoiding them and save radar points are received from the Frog.

## Backlog

![img_27.png](Sprint 2 Images/img_7.png)
![img_28.png](Sprint 2 Images/img.png)

All the items with their User Story can be seen in the [Appendix](Scrum%20documentation.md#Appendix)

## Meetings

### Meeting 6 - 29.05.2023 20:00

![img.png](Sprint 2 Images/img_4.png)
In this meeting, we discussed Sprint 2 and its items, as well as assigned sprint 2 tasks to team members.
This meeting was held via Discord on Monday.

### Meeting 7 - 31.05.2023 19:15

![img_26](Sprint 2 Images/img_11.png)
During this meeting, each team member presented their progress and discussed some issues encountered while working on
the project. This meeting was held via Discord on Wednesday.

### Meeting 8 - 2.06.2023 16:00

![img_26](Sprint 2 Images/img_5.png)
The meeting was held via Discord. Each team member discussed their progress and issue and tried to find a solution.

### Meeting 9 - 5.06.2023 21:00

![img_27](Sprint 2 Images/img_8.png)
We discussed our progress during this meeting, which was held via Discord, and everyone was almost done with some
of our tasks.

### Meeting 10 - 08.06.2023 13:30

![img_27](Sprint 2 Images/img_9.png)
The meeting was held via Discord. Slides were prepared for presentation, and our progress was discussed as well.

### Meeting 11 - 10.06.2023 18:30

![img_27](Sprint 2 Images/img_10.png)
Each member of the team made a retrospective for sprint 2 and discussed our sprint 3 goals. this meeting also was held
via Discord

## End Of Sprint
![](Sprint%203%20Images/37bc4327.png)
![](Sprint%203%20Images/img_1.png)

In general, the Sprint was successful as we managed to accomplish most of our tasks. The only exception was our request
for a map of the world with observed obstacles from The Pilot, which we decided to postpone to Sprint 3. The overall
outcome of the Sprint was positive, with everyone contributing to the project. We achieved our goal and are now eagerly
anticipating Sprint 3.

## Retrospectives

### Borislav

Sprint 2 was better than Sprint 1. We had more experience with GitLab, and also we used the feedback from Craig to make
our work better in terms of comments in the commits and our items on the board were more specific. Overall as a team I
think we continued to work hard and managed to complete a lot of tasks. On a personal note things could've gone better
because I worked the whole sprint with the radar points for the 2D map. It was annoying a little because I thought that
whatever I have created was not correct but then after debugging more times than I can count eventually Craig told us
that everything is correct. It was an opportunity for me to learn, but I definitely feel that maybe some time was wasted
on this issue. Overall a good Sprint everybody did a lot of things, we continued our good teamwork and communication and
I feel happy to be in the team.

### Viktor

During Sprint 2, most of the time was spent doing the collision detection system. Took quite a bit of trial and error
but in the end it turned out well. The frog currently detects obstacles at any angle, at any of the allowed speeds,
forward and backwards. There's a small flaw though. Which I believe will not cause any real issues. When you add an
angle to the command, it currently checks in a straight line to the destination. What it could possibly do better is
to check in a curved line, the calculator for that turned out to be too difficult, so I scratched that solution.
I believe everything connected with the teamwork went very well, and everyone did their work on time and well.

### Jafar

During the second sprint, our team could accomplish a lot in terms of The Frog development. We could successfully
implement features like the manual drive, active radar and check objects during driving, display position and
direction of the Frog and collisions. We also used the git effectively and scrum method by having regular stand-up
meetings to keep each other informed of our progress and any issues we encountered. We worked together to fix bugs
that arose. However, we faced some challenges, such as bugs with the map and the radar points and issues
with the collisions. it is important for us to improve our testing procedures to catch bugs and issues
earlier in the development process. We should also work on other must requirements during the sprint 3 to improve
our workflow and productivity.

### Bach

The collaboration with Jafar in enhancing the 2D map was a resounding success. We both actively contributed ideas and
proposed various approaches to tackle the problem at hand. Through rigorous testing and experimentation, we managed to
identify the most effective solutions, including leveraging the drawPolygon() function and utilizing PNG images for
direction indicators. Our joint efforts culminated in achieving a flawless outcome that exceeded our expectations.

On the other hand, the design phase for the mission function encountered some delays due to divergent opinions among
team members. It took considerable time to reach a compromise and settle on the current design. This experience
emphasized the importance of embracing flexibility, finding common ground, and prioritizing the project's requirements
over personal preferences.

Regrettably, I made an oversight by forgetting to commit the UI update for the mission function. This resulted in a
subsequent large commit that encompassed multiple changes, making it challenging to trace and review specific
modifications. To improve in the future, I should commit smaller, more focused tasks as they are completed, ensuring
better version control and traceability.

Additionally, we faced significant difficulties in resolving complex collision bugs.

In summary, to enhance future project outcomes, it is crucial to commit tasks incrementally, embrace collaboration and
flexibility, and promptly execute ideas while considering the project's requirements. By implementing these
improvements, we can streamline our workflow, mitigate challenges effectively, and achieve greater success in future
endeavors.

# Sprint 3 - Viktor is the scrum master for the first week && Bach is the scrum for the second week

During this Sprint, we expected to implement the rest of the functions (namely automatic moving, import/ export 
collision-point and mission log functions, ...). The completion of the documentation was also an important goal we need
to complete.

## Backlog

![](Sprint%203%20Images/d1e92fd3.png)
![](Sprint%203%20Images/0134181a.png)
![](Sprint%203%20Images/7b7fe50a.png)

## Meetings

### Meeting 12 - 12.06.2023 18:00

Meeting was held in class. We adjusted some things about the start for Sprint 3.
Each member discussed their progress and issues.
Forgot to take screenshot.

### Meeting 13 - 14.06.2023 18:00

![](Sprint%203%20Images/69c93936.png)
Meeting was held via Discord. We discussed our progress, and problems.

### Meeting 14 - 16.06.2023 18:00

![](Sprint%203%20Images/2f86d89e.png)
Meeting was held in class. Progress on the final backlog items was discussed
and issues with it.

### Meeting 15 - 19.06.2023 12:30

![](Sprint%203%20Images/eabb5f56.png)
Meeting was held in class. Receiving Craig's opinions about automatic movement.
Team members discussed testing all the remaining issues and fixing all
the remaining bugs.

### Meeting 16 - 22.06.2023 13:00

![](Sprint%203%20Images/6bf2d460.png)
Meeting was held on Discord. Every issue is finished, except for the two leftovers
that we decide it to be unnecessary to accomplish all the requirements.
Members discuss allocating documentation and video-making tasks.

### Meeting 17 - 23.06.2023 16:15

![](Sprint%203%20Images/a9993c90.png)
As every issue was completed during the last meeting, no screen_shot of issue boards is here.
A meeting was held in class with Craig, discussing if Craig could open our group project and run it
or not. It runs perfectly. Several documentation questions were asked by team members.

## Backlog items at the end of sprint 3
![](Sprint%203%20Images/01c20104.png)

## End Of Sprint
![](Sprint%203%20Images/23d2d8ee.png)
Overall, the sprint went really well, and we manage to complete all issues ahead of time (except for 2
issues that we decided were not necessary to work on).
Everyone did a great job and I believe we should all be proud.

## Retrospectives

### Borislav

Sprint 3 was an interesting sprint. This was the best sprint in terms of writing documentation and making our GitLab to
look as good as possible. We focused on making our program to meet all the requirements and making our documentation
to meet the requirements as well. I was able to complete the task that I couldn't in Sprint 2. Overall, I made the
Ground Control to be able to request the scanned radar points from The Pilot and also the importing and exporting radar
points to csv. I was the one in charge of creating the Demo Video. Overall, everybody did their tasks, and we continued
our good communication. I don’t think that I could’ve asked to be in a better team then this one. We had our stressful
moments, but I think that we were still able to have fun. We didn't encounter any problems during the sprint, so I think
that Sprint 3 was another successful sprint.

### Viktor

During the last sprint 3, we aimed to finish every leftover item in the backlog, and we succeeded.  Everyone 
pushed to finish their backlog issues. I started doing the automatic movement, but I was unable to finish it 
in the time I had planned. After that I asked Bach for help, and we finished it together. We completely changed 
the way I was going to it though. I was going for a full path finding system, but we realised that would be way 
more difficult, and take a lot longer.  So we switched it to a much lesser still working version. Currently it 
will rotate to the desired angle, and start moving until it detects an obstacle, or reaches its destination. If 
it detects and obstacle, it tries to go around it. Though I am not going to lie, I believe Bach did more work 
than me on it. The last few remaining days we, looked though the documentation and we updated and did whatever 
was missing.

### Bach

During Sprint 3, there were several positive outcomes. Firstly, the mission log was successfully completed, ensuring
accurate recording of mission details. Additionally, effective collaboration between myself and Viktor resulted in the
successful implementation of automatic movement. In terms of challenges or setbacks, no significant issues were
encountered, indicating a smooth development process. Looking ahead, there are no specific areas identified for
improvement at this stage, suggesting that the current workflow and strategies are working effectively. Overall, the
completion of the mission log and successful collaboration highlight the project's accomplishments, while the absence
of issues or improvements needed signifies a positive state of affairs.

### Jafar
Sprint 3 was a crucial sprint as it marked the end of our project. So, in this sprint, I worked on adding the ability
to export and import Ground Control missions to a CSV file, creating a database for the Pilot and saved collision points
in it, created another database for Ground Control and implemented the functionality to save collision points in that 
database as well. In terms of documentation, I documented the database design and also created a test traceability
matrix. Although we encountered some challenges during this sprint, we were able to overcome them. One notable issue
was related to the automatic movement of the Frog. Looking ahead, there are areas for improvement in our project.
Firstly, we could focus on enhancing the overall runtime efficiency of our system. This could involve optimizing 
algorithms and data structures to minimize execution time. Additionally, considering security measures and implementing
advanced database techniques would be beneficial for ensuring the integrity and confidentiality of our data. 
## Appendix

### Sprint 1

![img_1.png](Appendix/Backlog-Sprint-1.png)
![img_11.png](Appendix/img_11.png)
![img_12.png](Appendix/img_12.png)
![img_13.png](Appendix/img_13.png)
![img_14.png](Appendix/img_14.png)
![img_15.png](Appendix/img_15.png)
![img.png](Appendix/Backlog-Sprint1-2.png)
![img_2.png](Appendix/Backlog-Sprint-3.png)
![img_3.png](Appendix/Backlog-Sprint-4.png)

### Sprint 2

![img_24.png](Appendix/img_1.png)
![img_25.png](Appendix/img_2.png)
![img_26.png](Appendix/img_3.png)
![img_28.png](Appendix/img_4.png)
![img_27.png](Appendix/img.png)
![img_29.png](Appendix/img_5.png)
![img_30.png](Appendix/img_6.png)
![img_30.png](Appendix/img_7.png)

### Sprint 3

![](Appendix/img_8.png)
![](Appendix/img_9.png)
![](Appendix/img_10.png)
![](Appendix/img_16.png)
![](Appendix/a196f326.png)
![](Appendix/ab695849.png)
![](Appendix/6c82c147.png)
![](Appendix/c40b3191.png)
![](Appendix/67d8cbf3.png)
