package nl.saxion.ptbc.ground_control.automaticMovement;

import nl.saxion.ptbc.ground_control.GroundControlPanel;

import java.awt.geom.Point2D;

/**
 * Makes the frog move to a destination, if possible , avoiding obstacles.
 */
public class AutomaticMovement {
    private GroundControlPanel groundControlPanel;
    public static boolean stopAutomatic = true;
    private static Point2D.Double start;
    private static double newCurrentAngle;
    private final double oneLoopDriveAngle = 6;
    private final int power = 1000;
    private final int angle = 5;
    private final int duration = 1;

    public AutomaticMovement(GroundControlPanel groundControlPanel, Point2D.Double start, Point2D.Double destination, double currentAngle) {
        this.groundControlPanel = groundControlPanel;
        System.out.println("Line 14, Start is: " + start + " | Destination is " + destination + " | currentAngle is " + currentAngle);
        stopAutomatic = false;
        changeDirection(start, destination, currentAngle);
        move(this.start, destination);
    }

    private void changeDirection(Point2D.Double start, Point2D.Double destination, double currentAngle) {
        calculateAngle(start, destination, currentAngle);
    }

    /**
     * Calculate the angle which the frog needs to rotate to face a position.
     * @param start the frogs position.
     * @param destination the new position.
     * @param currentAngle the frogs current angle.
     */
    private void calculateAngle(Point2D.Double start, Point2D.Double destination, double currentAngle) {
        double xD = destination.getX() - start.getX();
        double zD = destination.getY() - start.getY();

        double rads = Math.atan2(xD, zD);
        double degs = Math.toDegrees(rads);
        System.out.println("Line 29, Degrees After Atan2: " + degs);

        turnFrogToAngle(currentAngle, degs);
    }

    /**
     * Send turning commands to the frog to rotate it.
     * @param startAngle the starting angle
     * @param degs the angle to be added to the current angle.
     */
    private void turnFrogToAngle(double startAngle, double degs) {
        if (degs < 0)
            degs = 360 + degs;

        System.out.println("second degs:  " + degs);


        if (startAngle < degs) {
            double angleToTurn = degs - startAngle;
            if (angleToTurn < 180) {
                turnClockwise(angleToTurn);
            } else if (angleToTurn > 180) {
                turnCounterClockwise(360 - angleToTurn);
            }
        } else if (startAngle > degs) {
            double angleToTurn = startAngle - degs;
            if (angleToTurn < 180) {
                turnCounterClockwise(angleToTurn);
            } else if (angleToTurn > 180) {
                turnClockwise(360 - angleToTurn);
            }
        } else if (startAngle == degs) {
            //Drive straight

        } else if (Math.abs(degs - startAngle) == 180) {
            //turn around
            turnClockwise(180);
        }

    }


    /**
     * Sends commands to the frog to turn clockwise until it has rotated to the desired angle
     * @param angleToTurn the angle difference it needs to adjust for.
     */
    private void turnClockwise(double angleToTurn) {
        double loopNumb = angleToTurn / oneLoopDriveAngle;
        boolean extra = false;
        double rounded = Math.round(loopNumb);

        if (loopNumb + 0.5 < rounded) {
            rounded -= 1;
        }

        if (angleToTurn % oneLoopDriveAngle < (oneLoopDriveAngle / 2 + 1)) {
            rounded += 1;
            extra = true;
        }
        System.out.println("loop numb: " + rounded + " Angle: " + angleToTurn);
        for (int i = 0; i < rounded; i++) {
            groundControlPanel.sendMessageToPilot(driveCommand(power, angle, duration));
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (extra && i == rounded - 1) {
                break;
            }

            groundControlPanel.sendMessageToPilot(driveCommand(-power, -angle, duration));
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println(i);

        }
    }


    /**
     * Sends commands to the frog to turn counterclockwise until it has rotated to the desired angle
     * @param angleToTurn the angle difference it needs to adjust for.
     */
    private void turnCounterClockwise(double angleToTurn) {
        double loopNumb = angleToTurn / oneLoopDriveAngle;
        boolean extra = false;
        double rounded = Math.round(loopNumb);

        if (loopNumb + 0.5 < rounded) {
            rounded -= 1;
        }

        if (angleToTurn % oneLoopDriveAngle > (oneLoopDriveAngle / 2 + 1)) {
            rounded += 1;
            extra = true;
        }
        System.out.println("loop numb: " + rounded + " Angle: " + angleToTurn);
        for (int i = 0; i < rounded; i++) {
            groundControlPanel.sendMessageToPilot(driveCommand(power, -angle, duration));
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            if (extra && i == rounded - 1) {
                break;
            }

            groundControlPanel.sendMessageToPilot(driveCommand(-power, angle, duration));
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }

    /**
     * Sends commands to the frog to move forward, with no angle, from a starting position, to end position, unless it encounters a collision on the way.
     * @param start the starting position (the frogs position)
     * @param destination the end destination
     */
    private void move(Point2D.Double start, Point2D.Double destination) {
        double differenceX = destination.getX() - start.getX();
        double differenceZ = destination.getY() - start.getY();

        double totalDistance = Math.sqrt(Math.pow(differenceX, 2) + Math.pow(differenceZ, 2));

        double loopNumber = totalDistance / 2.3;

        System.out.println("Number of loops: " + loopNumber + " | Distance is: " + totalDistance);

        for (int i = 0; i < loopNumber; i++) {

            System.out.println("Stop: " + stopAutomatic);

            if (stopAutomatic) {
                break;
            }

            groundControlPanel.sendMessageToPilot(driveCommand(power, 0, duration));
            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
            System.out.println(i);
        }

        if (stopAutomatic) {
            dodge(destination);
        }
    }

    /**
     * Makes the frog move backwards and turn 33 degrees to the right, to attempt to avoid the obstacle.
     * @param destination the destination the frog is going for
     */
    private void dodge(Point2D.Double destination) {
        groundControlPanel.sendMessageToPilot(driveCommand(-1000, 0, 2));

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

        turnCounterClockwise(33);

        for (int i = 0; i < 15; i++) {
            groundControlPanel.sendMessageToPilot(driveCommand(power, 0, duration));

            try {
                Thread.sleep(3500);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }

            System.out.println("i: " + i);
        }

        stopAutomatic = false;
        Point2D.Double newStart = new Point2D.Double(start.x, start.y);
        new AutomaticMovement(groundControlPanel, newStart, destination, newCurrentAngle);
    }

    public static void updateStartPos(Point2D.Double newStart) {
        start = newStart;
    }

    public static void updateStartAngle(double currentAngle) {
        newCurrentAngle = currentAngle;
    }

    private String driveCommand(int power, int angle, int duration) {
        return String.format("PILOT DRIVE %d %d %d", power, angle, duration);
    }

}
