package nl.saxion.ptbc.ground_control.storageHandler;

import nl.saxion.ptbc.dbUtil.dbConnection;
import nl.saxion.ptbc.shared.Mission;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import nl.saxion.ptbc.ObjectDisplay.Point2D;
/**
 * This class handles the interaction with the database for saving data.
 */
public class DatabaseHandler {
    private static DatabaseHandlerListener listener;

    /**
     * Sets the listener for database events.
     *
     * @param listener The listener to set.
     */
    public static void setListener(DatabaseHandlerListener listener) {
        DatabaseHandler.listener = listener;
    }

    /**
     * Saves a mission to the database.
     *
     * @param mission The mission to save.
     */
    public static void saveMissionToDatabase(Mission mission) {
        try (Connection connection = dbConnection.getConnection()) {
            int newMissionId = 0;
            try (PreparedStatement maxMissionIdStatement = connection.prepareStatement("SELECT MAX(mission_id) FROM Mission");
                 ResultSet resultSet = maxMissionIdStatement.executeQuery()) {
                if (resultSet.next()) {
                    newMissionId = resultSet.getInt(1) + 1;
                }
            }

            String insertQuery = "INSERT INTO Mission (mission_id, dataTime, description, mission_name) VALUES (?, ?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                statement.setInt(1, newMissionId);
                statement.setString(2, LocalDateTime.now().toString());
                statement.setString(3, mission.getDescription());
                statement.setString(4, mission.getName());
                statement.executeUpdate();
            }

            if (listener != null) {
                listener.onMissionSaved("Mission saved successfully!");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

    /**
     * Saves the current mission log to the database.
     *
     * @param missionLogText The mission log text to save.
     * @throws SQLException if there's an error executing the SQL statement.
     */
    public static void saveCurrentMissionToDatabase(String missionLogText) throws SQLException {
        try (Connection connection = dbConnection.getConnection()) {
            int newMissionId = 0;
            try (PreparedStatement maxMissionIdStatement = connection.prepareStatement("SELECT MAX(mission_id) FROM Drivecommands");
                 ResultSet resultSet = maxMissionIdStatement.executeQuery()) {
                if (resultSet.next()) {
                    newMissionId = resultSet.getInt(1) + 1;
                }
            }

            int newDcId = getDcIdFromMessage();

            String insertQuery = "INSERT INTO Drivecommands ( dc_id, message,mission_id) VALUES (?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                statement.setInt(3, newMissionId);
                statement.setString(2, missionLogText);
                statement.setInt(1, newDcId);
                statement.executeUpdate();
            }

            if (listener != null) {
                listener.onMissionSaved("Current mission saved successfully!");
            }
        }
    }
    /**
     * Retrieves the next available Drive Command ID from the database.
     *
     * @return The next available dc_id.
     * @throws SQLException if there's an error executing the SQL statement.
     */
    private static int getDcIdFromMessage() throws SQLException {
        try (Connection connection = dbConnection.getConnection()) {
            int maxDcId = 0;
            try (PreparedStatement maxDcIdStatement = connection.prepareStatement("SELECT MAX(dc_id) FROM DriveCommands");
                 ResultSet resultSet = maxDcIdStatement.executeQuery()) {
                if (resultSet.next()) {
                    maxDcId = resultSet.getInt(1);
                }
            }
            return maxDcId + 1;
        }
    }

    /**
     * Retrieves the highest mission_id from the database.
     *
     * @return The highest mission_id.
     */
    public static int getMissionIdFromDatabase() {
        try (Connection connection = dbConnection.getConnection()) {
            int missionId = 0;
            String selectQuery = "SELECT MAX(mission_id) FROM Mission";
            try (PreparedStatement statement = connection.prepareStatement(selectQuery);
                 ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    missionId = resultSet.getInt(1);
                }
            }
            return missionId;
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }
    /**
     * Saves collision points to the database.
     *
     * @param radarPoints The list of radar points to save.
     * @param missionId   The mission ID associated with the radar points.
     */
    public static void saveCollisionPointsToDatabase(ArrayList<Point2D> radarPoints, int missionId) {
        try (Connection connection = dbConnection.getConnection()) {
            // Clear existing radar points from the table
            String deleteQuery = "DELETE FROM RadarPoint";
            try (PreparedStatement deleteStatement = connection.prepareStatement(deleteQuery)) {
                deleteStatement.executeUpdate();
            }
            String insertQuery = "INSERT INTO RadarPoint (radar_id, x, z, mission_id) VALUES (?, ?, ?, ?)";
            try (PreparedStatement statement = connection.prepareStatement(insertQuery)) {
                for (int i = 0; i < radarPoints.size(); i++) {
                    Point2D radarPoint = radarPoints.get(i);
                    statement.setInt(1, i + 1); // Set the radar_id as the index (1-based)
                    statement.setDouble(2, radarPoint.getX());
                    statement.setDouble(3, radarPoint.getZ());
                    statement.setInt(4, missionId);
                    statement.executeUpdate();
                }
            }

            if (listener != null) {
                listener.onMissionSaved("Collision points saved successfully!");
            }
        } catch (SQLException ex) {
            throw new RuntimeException(ex);
        }
    }

}
