package nl.saxion.ptbc.ground_control.storageHandler;


import nl.saxion.ptbc.ObjectDisplay.Point2D;
import nl.saxion.ptbc.shared.Mission;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * This class handles the reading and exporting of missions and radar points to CSV files.
 */
public class CSVHandler {
    private static final String FILE_EXTENSION = ".csv";
    private static final String DATE_FORMAT = "dd-MM-yyyy HH-mm";
    private static final String HEADER_LINE_RADAR_POINTS = "coordinate x, coordinate z";

    /**
     * Reads missions from a CSV file and returns a list of Mission objects.
     *
     * @param file The CSV file containing the missions.
     * @return A list of Mission objects read from the CSV file.
     */
    public static List<Mission> readMissionsFromCSV(File file) {
        List<Mission> missions = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            boolean isFirstLine = true;
            while ((line = reader.readLine()) != null) {
                if (isFirstLine) {
                    isFirstLine = false;
                    continue; // Skip the first line
                }

                String[] data = line.split(",");
                Mission mission = new Mission();
                mission.setMissionId(Integer.parseInt(data[0]));
                mission.setDateTime(data[1]);
                mission.setDescription(data[2]);
                mission.setName(data[3]);
                for (int i = 4; i < data.length; i++) {
                    mission.addCommand(data[i]);
                }
                missions.add(mission);
            }
        } catch (IOException | NumberFormatException e) {
            e.printStackTrace();
        }
        return missions;
    }

    /**
     * Exports a mission to a CSV file.
     *
     * @param mission The mission to export.
     */
    public static void exportMissionToCSV(Mission mission) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
        String dateTime = dateFormat.format(new Date());

        String fileName = "mission_" + dateTime + FILE_EXTENSION;

        try (PrintWriter writer = new PrintWriter(new FileWriter(fileName))) {
            // Write CSV header
            writer.println("mission_id,datetime,description,mission_name,mission_command");

            // Write mission details
            writer.write(String.valueOf(mission.getMissionId()));
            writer.write(",");
            writer.write(dateTime);
            writer.write(",");
            writer.write(mission.getDescription());
            writer.write(",");
            writer.write(mission.getName());

            for (String command : mission.getDriveCommands()) {
                writer.write(",");
                writer.write(command);
            }

            writer.println();
        } catch (IOException ex) {
            ex.printStackTrace();
            throw new RuntimeException("Failed to export mission.");
        }
        File file = new File(fileName);
        String absolutePath = file.getAbsolutePath();
        System.out.println("CSV file saved at: " + absolutePath);
    }

    /**
     * Exports radar points to a CSV file.
     *
     * @param points the list of radar points to export
     */
    public static void exportRadarPointsToCSV(List<Point2D> points) {
        String filename = generateFilename();
        try (FileWriter writer = new FileWriter(filename)) {
            writer.write(HEADER_LINE_RADAR_POINTS);
            writer.write(System.lineSeparator());

            for (Point2D point : points) {
                writer.write("RADAR POINT " + point.getX() + ", " + point.getZ());
                writer.write(System.lineSeparator());
            }

            System.out.println("CSV file exported successfully: " + filename);
        } catch (IOException e) {
            System.out.println("Error exporting CSV file: " + e.getMessage());
        }
    }

    /**
     * Reads radar points from a CSV file.
     *
     * @param file the CSV file to read points from
     * @return the list of radar points read from the file
     */
    public static ArrayList<Point2D> readPointsFromFile(File file) {
        ArrayList<Point2D> points = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("RADAR POINT")) {
                    String[] splitLine = line.split(",");
                    double x = Double.parseDouble(splitLine[0].split(" ")[2]);
                    double z = Double.parseDouble(splitLine[1]);
                    points.add(new Point2D(x, z));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return points;
    }

    /**
     * Generates a filename for the CSV file based on the current timestamp.
     *
     * @return the generated filename
     */
    private static String generateFilename() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        String timestamp = dateFormat.format(new Date());
        return "Radar Points - " + timestamp + FILE_EXTENSION;
    }

}



