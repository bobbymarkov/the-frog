package nl.saxion.ptbc.ground_control;
import nl.saxion.ptbc.shared.Shared;
import nl.saxion.ptbc.sockets.SasaClient;
import nl.saxion.ptbc.sockets.SasaCommunicationException;
import nl.saxion.ptbc.sockets.SasaConnectionException;
import nl.saxion.ptbc.sockets.SasaSocketInformation;

/**
 * The GroundControl class represents the base class for ground control operations.
 */
public class GroundControl implements SasaSocketInformation {
    private final SasaClient sasaClient;
    private static GroundControlDashboard groundControlDashboard;

    /**
     * Constructs a GroundControl instance and connects to the specified host and port.
     * @param host the host address to connect to
     * @param port the port number to connect to
     * @throws SasaConnectionException if a connection error occurs
     */
    public GroundControl(String host, int port) throws SasaConnectionException {
        // Setup server
        sasaClient = new SasaClient();
        sasaClient.subscribe(this);
        sasaClient.connect(host, port);
        System.out.println("Ground Control is connected to " + host + ":" + port);
    }

    /**
     * The main method that starts the Ground Control application.
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        GroundControl groundControl;
        try {
            // Setup Ground Control as client of The Pilot
            groundControl = new GroundControl(Shared.COMMUNICATION_SERVER, Shared.COMMUNICATION_PORT);

            // Start the Ground Control dashboard
            groundControlDashboard = new GroundControlDashboard(groundControl);
            groundControlDashboard.start();

        } catch (SasaConnectionException e) {
            System.err.println("Connection error: " + e.getMessage());
        }
    }

    @Override
    public void receivedData(String message) {
        groundControlDashboard.updateInformation(message);
    }

    @Override
    public void statusUpdate(String client, String status) {
        System.out.printf("[State] %s: %s%n", client, status);
    }

    /**
     * Send message from ground control to pilot
     * @param text message to send
     * @param groundControl to access the current running ground control instance and make it the one to send
     */
    public static void sendMessageToPilot(String text, GroundControl groundControl) {
        try {
            groundControl.sasaClient.send("[GROUND CONTROL] " + text);
        } catch (SasaCommunicationException e) {
            throw new RuntimeException(e);
        }
    }

}
