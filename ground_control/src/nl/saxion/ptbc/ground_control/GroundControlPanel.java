package nl.saxion.ptbc.ground_control;

import nl.saxion.ptbc.ground_control.automaticMovement.AutomaticMovement;
import nl.saxion.ptbc.ground_control.automaticMovement.MapListener;
import nl.saxion.ptbc.ground_control.storageHandler.CSVHandler;
import nl.saxion.ptbc.ground_control.storageHandler.DatabaseHandler;
import nl.saxion.ptbc.ground_control.storageHandler.DatabaseHandlerListener;
import nl.saxion.ptbc.shared.Direction;
import nl.saxion.ptbc.shared.Mission;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.*;
import javax.imageio.ImageIO;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static nl.saxion.ptbc.shared.Shared.*;

/**
 * The GroundControlPanel class represents the panel for ground control in the Ground Control System.
 * It provides controls and displays for managing missions and communicating with the pilot.
 */
public class GroundControlPanel extends JPanel implements DatabaseHandlerListener {
    private GroundControlDashboard groundControlDashboard;
    //7 TextField
    private TextField xTextField;
    private TextField zTextField;
    private TextField powerTextField;
    private TextField angleTextField;
    private TextField durationTextField;
    private TextField descriptionTextField;
    private TextField nameTextField;

    //11 Buttons
    private JButton goButton;
    private JButton addCommandButton;
    private JButton addMissionButton;
    private JButton saveCurrentMissionButton;
    private JButton importMissionButton;
    private JFileChooser fileChooser;
    private JButton exportMissionButton;
    private JButton requestDetectedObstaclesButton;
    private JButton saveCollisionPointMapButton;
    private JButton importCollisionPointMapButton;
    private JButton exportCollisionPointMapButton;
    private JButton sendMissionToPilotButton;

    //Map
    private BufferedImage mapImage;
    private Direction direction;
    private List<Point2D.Double> frogPath;
    private double previousAngle = STARTING_ANGLE;
    private Rectangle2D.Double clickZone;
    public Point2D.Double destination = new Point2D.Double(-50, -50);

    //Log
    private JTextArea missionLog;
    private Mission mission;
    private JTextArea driveCommandsList;
    private final String startingTexts = "Power: 1000, -1000, 2000, -2000\nAngle: 0, 20, -20\nDuration: 1, 2\nDrive command list:\n";
    private JScrollPane scrollPane;

    private ArrayList<nl.saxion.ptbc.ObjectDisplay.Point2D> allRadarPoints = new ArrayList<>();

    /**
     * Initialize the ground control panel and link it to which ever dashboard that created it to access the elements and function inside the dashboard
     *
     * @param groundControlDashboard the dashboard can use "this" key word to add itself to the param.
     */
    public GroundControlPanel(GroundControlDashboard groundControlDashboard) {
        //Init main elements
        this.groundControlDashboard = groundControlDashboard;
        MapListener mapListener = new MapListener(this);
        frogPath = new ArrayList<>();
        frogPath.add(new Point2D.Double(FROG_STARTING_POSITION_X, FROG_STARTING_POSITION_Z));

        addMouseListener(mapListener);
        setPanelSize();
        setLayout(null);
        addComponents();
        setupButtonListeners();
    }


    /**
     * Add function to all the buttons
     */
    private void setupButtonListeners() {
        addMissionButton.addActionListener(e -> {
            mission = new Mission();

            //Active
            nameTextField.setEnabled(true);
            nameTextField.setText("Name");

            descriptionTextField.setEnabled(true);
            descriptionTextField.setText("Description");

            powerTextField.setEnabled(true);
            powerTextField.setText("Power");

            angleTextField.setEnabled(true);
            angleTextField.setText("Angle");

            durationTextField.setEnabled(true);
            durationTextField.setText("Duration");

            addCommandButton.setEnabled(true);
            sendMissionToPilotButton.setEnabled(true);

            scrollPane.setEnabled(true);
            driveCommandsList.setText(startingTexts);
        });

        addCommandButton.addActionListener(e -> {
            if (checkInputMissionTextField(powerTextField, "1000 -1000 2000 -2000") && checkInputMissionTextField(angleTextField, "0 20 -20") && checkInputMissionTextField(durationTextField, "1 2")) {
                String driveMessage = String.format("PILOT DRIVE %d %d %d", Integer.parseInt(powerTextField.getText()), Integer.parseInt(angleTextField.getText()), Integer.parseInt(durationTextField.getText()));
                mission.addCommand(driveMessage);
                driveCommandsList.setText(startingTexts);
                for (String dm : mission.getDriveCommands())
                    driveCommandsList.append(dm + "\n");
            }
        });

        saveCurrentMissionButton.addActionListener(e -> {
            ArrayList<String> commands = mission.getDriveCommands();
            String[] lines = commands.toArray(new String[0]);
            for (String line : lines) {
                if (line.contains("PILOT DRIVE")) {
                    try {
                        saveCurrentMissionToDatabase(line.trim());
                    } catch (SQLException ex) {
                        throw new RuntimeException(ex);
                    }
                }
            }
        });

        exportMissionButton.addActionListener(e -> {
            if (mission != null) {
                exportMissionToCSV(mission);
                JOptionPane.showMessageDialog(this, "Mission exported successfully!", "Export Mission", JOptionPane.INFORMATION_MESSAGE);
            }
        });
        importMissionButton.addActionListener(e -> importMission());
        fileChooser = new JFileChooser();


        saveCollisionPointMapButton.addActionListener(e -> addToChatLog("Collision points saved to the database successfully!"));


        sendMissionToPilotButton.addActionListener(e -> {
            if (nameTextField.getText().isBlank()) {
                nameTextField.setText("Must not be blank!");
            } else if (descriptionTextField.getText().isBlank()) {
                descriptionTextField.setText("Must not be blank!");
            } else if (mission.getDriveCommands() == null || mission.getDriveCommands().isEmpty()) {
                driveCommandsList.setText(startingTexts);
                driveCommandsList.append("PLEASE ADD COMMANDS!");
            } else {
                mission.setName(nameTextField.getText());
                mission.setDescription(descriptionTextField.getText());
                //Save mission
                saveMissionToDatabase();
                sendMissionToPilot();

                //De-active
                nameTextField.setEnabled(false);
                nameTextField.setText("");

                descriptionTextField.setEnabled(false);
                descriptionTextField.setText("");

                powerTextField.setEnabled(false);
                powerTextField.setText("");

                angleTextField.setEnabled(false);
                angleTextField.setText("");

                durationTextField.setEnabled(false);
                durationTextField.setText("");

                addCommandButton.setEnabled(false);
                sendMissionToPilotButton.setEnabled(false);

                scrollPane.setEnabled(false);
                driveCommandsList.setText("");
            }
        });

        requestDetectedObstaclesButton.addActionListener(e -> sendMessageToPilot("Ground Control Requests Detected Obstacles"));

        exportCollisionPointMapButton.addActionListener(e -> CSVHandler.exportRadarPointsToCSV(allRadarPoints));

        importCollisionPointMapButton.addActionListener(e -> importRadarPoints());

        goButton.addActionListener(e -> {
            if (checkInputXZTextField(xTextField, zTextField)) {
                double xActualCor = Double.parseDouble(xTextField.getText().replace(',', '.'));
                double zActualCor = Double.parseDouble(zTextField.getText().replace(',', '.'));
                Point2D.Double actualDes = new Point2D.Double(xActualCor, zActualCor);
                moveAutomaticallyToPoint(actualDes);
                Point2D.Double posOnGroundControl = calculatePosition(actualDes.x, actualDes.y);
                destination = new Point2D.Double(posOnGroundControl.x + TILE_SIZE, posOnGroundControl.y + TILE_SIZE);
            }
        });

        fileChooser = new JFileChooser();
    }

    /**
     * Check if user inputs correctly data (x needs to be from -78.75 to 421.29, and z needs to be from -145.89 to 354.13)
     *
     * @param xTextField TextField of real 3D map x coordinate
     * @param zTextField TextField of real 3D map z coordinate
     * @return true if the value of these TextFields is in correct range, false otherwise
     */
    private boolean checkInputXZTextField(TextField xTextField, TextField zTextField) {
        double xActualCor = Double.parseDouble(xTextField.getText().replace(',', '.'));
        double zActualCor = Double.parseDouble(zTextField.getText().replace(',', '.'));

        xTextField.setForeground(Color.red);
        if (MAP_2D_BOTTOM_LEFT_X < xActualCor && xActualCor < MAP_2D_TOP_RIGHT_X) {
            xTextField.setForeground(Color.black);
            if (MAP_2D_BOTTOM_LEFT_Z < zActualCor && zActualCor < MAP_2D_TOP_RIGHT_Z) {
                zTextField.setForeground(Color.black);
                return true;
            }

            zTextField.setForeground(Color.red);
        }

        return false;
    }

    /**
     * Check the input of the TextField to prevent wrong input from users
     *
     * @param randomTextField which TextField to check
     * @param possibleAnswers All the possible answers that users can put in, all should be put in a string, separated by a space
     * @return return true if users input right value, false if wrong value
     */
    private boolean checkInputMissionTextField(TextField randomTextField, String possibleAnswers) {
        if (Arrays.stream(possibleAnswers.split(" ")).anyMatch(pA -> pA.equals(randomTextField.getText()))) {
            randomTextField.setForeground(Color.black);
            return true;
        }

        randomTextField.setForeground(Color.red);
        return false;
    }

    /**
     * Imports a mission from a CSV file using a file chooser.
     */
    private void importMission() {
        int returnVal = fileChooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            List<Mission> importedMissions = CSVHandler.readMissionsFromCSV(file);
            if (!importedMissions.isEmpty()) {
                handleImportedMissions(importedMissions);
            } else {
                JOptionPane.showMessageDialog(this, "No missions found in the CSV file.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Imports radar points from a file selected by the user using a file chooser dialog.
     * The radar points are sent to the pilot and added to the list of all radar points.
     * If radar points are successfully imported, the dashboard is repainted.
     */
    private void importRadarPoints() {
        int returnVal = fileChooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            ArrayList<nl.saxion.ptbc.ObjectDisplay.Point2D> newRadarPoints = CSVHandler.readPointsFromFile(file);
            groundControlDashboard.sendMessageToPilot(nl.saxion.ptbc.ObjectDisplay.Point2D.convertPointListToString(newRadarPoints));
            if (!newRadarPoints.isEmpty()) {
                allRadarPoints.addAll(newRadarPoints);
                groundControlDashboard.addToAllRadarPoints(newRadarPoints);
                repaint();
            } else {
                JOptionPane.showMessageDialog(this, "No radar points found in the file.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    /**
     * Handles the imported missions by assigning each mission to the 'mission' variable and sending it to the pilot.
     *
     * @param importedMissions The list of imported missions
     */
    private void handleImportedMissions(List<Mission> importedMissions) {
        for (Mission importedMission : importedMissions) {
            mission = importedMission;
            sendMissionToPilot();
        }
    }

    /**
     * Exports a mission to a CSV file.
     *
     * @param mission The mission to export
     */
    private void exportMissionToCSV(Mission mission) {
        CSVHandler.exportMissionToCSV(mission);
    }

    /**
     * Sends the mission to the pilot by executing drive commands and adding delays between commands.
     */
    private void sendMissionToPilot() {
        for (String command : mission.getDriveCommands()) {
            sendMessageToPilot(command);
            long duration = (long) Integer.parseInt(command.split(" ")[4]) * 1000;
            try {
                Thread.sleep(duration + 1000);
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

    }

    /**
     * Saves the mission to the database.
     */
    private void saveMissionToDatabase() {
        DatabaseHandler.setListener(this);
        DatabaseHandler.saveMissionToDatabase(mission);
    }

    /**
     * Saves the current mission (containing "Drive") to the database.
     *
     * @param missionLogText The mission log text to save
     * @throws SQLException If an SQL error occurs
     */
    private void saveCurrentMissionToDatabase(String missionLogText) throws SQLException {
        DatabaseHandler.setListener(this);
        DatabaseHandler.saveCurrentMissionToDatabase(missionLogText);
    }

    /**
     * Callback method invoked when a mission is successfully saved to the database.
     *
     * @param message The message indicating the success of the mission save
     */
    @Override
    public void onMissionSaved(String message) {
        addToChatLog(message);
    }

    /**
     * Sends a message to the pilot and adds sent messages to the chat log.
     *
     * @param messageToSend The message to send to the pilot
     */
    public void sendMessageToPilot(String messageToSend) {
        groundControlDashboard.sendMessageToPilot(messageToSend); // Pass the message to the dashboard
        addToChatLog("[Sent] " + messageToSend);
    }

    /**
     * Adds a message to the chat log.
     *
     * @param message The message to add to the chat log
     */
    public void addToChatLog(String message) {
        missionLog.insert(message + "\n", 0);
    }

    /**
     * Place to create all TextFields
     */
    private void addTextFields() {
        //7 TextFields
        xTextField = createAndAddTextField("X", 33 * TILE_SIZE, TILE_SIZE, 10 * TILE_SIZE, 3 * TILE_SIZE, true);

        zTextField = createAndAddTextField("Z", 44 * TILE_SIZE, TILE_SIZE, 10 * TILE_SIZE, 3 * TILE_SIZE, true);

        powerTextField = createAndAddTextField("", 80 * TILE_SIZE, 25 * TILE_SIZE, 5 * TILE_SIZE, 3 * TILE_SIZE, false);

        angleTextField = createAndAddTextField("", 86 * TILE_SIZE, 25 * TILE_SIZE, 5 * TILE_SIZE, 3 * TILE_SIZE, false);

        durationTextField = createAndAddTextField("", 92 * TILE_SIZE, 25 * TILE_SIZE, 5 * TILE_SIZE, 3 * TILE_SIZE, false);

        descriptionTextField = createAndAddTextField("", 80 * TILE_SIZE, 5 * TILE_SIZE, 17 * TILE_SIZE, 3 * TILE_SIZE, false);

        nameTextField = createAndAddTextField("", 80 * TILE_SIZE, TILE_SIZE, 17 * TILE_SIZE, 3 * TILE_SIZE, false);
    }

    /**
     * Place to create all buttons
     */
    private void addButtons() {
        //10 Buttons
        goButton = createAndAddButton("GO", 55 * TILE_SIZE, TILE_SIZE, 6 * TILE_SIZE, 3 * TILE_SIZE);

        addMissionButton = createAndAddButton("Add mission", 33 * TILE_SIZE, 5 * TILE_SIZE, 13 * TILE_SIZE, 3 * TILE_SIZE);

        saveCurrentMissionButton = createAndAddButton("Save current mission", 48 * TILE_SIZE, 5 * TILE_SIZE, 13 * TILE_SIZE, 3 * TILE_SIZE);

        importMissionButton = createAndAddButton("Import mission", 33 * TILE_SIZE, 9 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        exportMissionButton = createAndAddButton("Export mission", 33 * TILE_SIZE, 13 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        requestDetectedObstaclesButton = createAndAddButton("Request detected obstacles from Frog", 33 * TILE_SIZE, 17 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        saveCollisionPointMapButton = createAndAddButton("Save collision-point map", 33 * TILE_SIZE, 21 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        importCollisionPointMapButton = createAndAddButton("Import collision-point map", 33 * TILE_SIZE, 25 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        exportCollisionPointMapButton = createAndAddButton("Export collision-point map", 33 * TILE_SIZE, 29 * TILE_SIZE, 28 * TILE_SIZE, 3 * TILE_SIZE);

        addCommandButton = createAndAddButton("Add command", 80 * TILE_SIZE, 29 * TILE_SIZE, 8 * TILE_SIZE, 3 * TILE_SIZE);
        addCommandButton.setEnabled(false);

        sendMissionToPilotButton = createAndAddButton("Send mission to pilot", 89 * TILE_SIZE, 29 * TILE_SIZE, 8 * TILE_SIZE, 3 * TILE_SIZE);
        sendMissionToPilotButton.setEnabled(false);
    }

    /**
     * Adds the components (text fields, buttons, map, etc.) to the panel.
     */
    private void addComponents() {
        addTextFields();
        addButtons();

        //Map
        loadMapImage();
        direction = new Direction("DirectionEdited2.png"); // set up direction
        clickZone = new Rectangle2D.Double(TILE_SIZE, TILE_SIZE, MAP_2D_UI_WIDTH_GROUND_CONTROL, MAP_2D_UI_HEIGHT_GROUND_CONTROL);

        //JTextArea
        missionLog = new JTextArea(); // Action Log
        missionLog.setBounds(62 * TILE_SIZE, TILE_SIZE, 17 * TILE_SIZE, 31 * TILE_SIZE);
        add(missionLog);

        //Mission drive commands display zone
        driveCommandsList = new JTextArea();
        scrollPane = new JScrollPane(driveCommandsList);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setBounds(80 * TILE_SIZE, 9 * TILE_SIZE, 17 * TILE_SIZE, 15 * TILE_SIZE);
        driveCommandsList.setEditable(false);
        scrollPane.setEnabled(false);
        add(scrollPane);
    }

    /**
     * Init TextField, set its position, set its enabled status and add it to the ground control panel
     *
     * @param content inner text to show what should be input inside
     * @param x       x coordinate on UI
     * @param y       y coordinate on UI
     * @param width   width of the TextField
     * @param height  height of the TextField
     * @param enabled enabled status
     * @return return the temporary created TextField that stores the data.
     */

    private TextField createAndAddTextField(String content, int x, int y, int width, int height, boolean enabled) {
        TextField temp = new TextField(content);
        temp.setBounds(x, y, width, height);
        temp.setEnabled(enabled);
        add(temp);
        return temp;
    }

    /**
     * Init button, set its position, set its enabled status and add it to the ground control panel
     *
     * @param innerText inner text to show what should be input inside
     * @param x         x coordinate on UI
     * @param y         y coordinate on UI
     * @param width     width of the button
     * @param height    height of the button
     * @return return the temporary created button that stores the data.
     */
    private JButton createAndAddButton(String innerText, int x, int y, int width, int height) {
        JButton temp = new JButton(innerText);
        temp.setBounds(x, y, width, height);
        add(temp);
        return temp;
    }

    /**
     * Sets the size of the panel.
     */
    private void setPanelSize() {
        Dimension size = new Dimension(GROUND_CONTROL_SCREEN_WIDTH, GROUND_CONTROL_SCREEN_HEIGHT);
        setPreferredSize(size);
    }

    /**
     * Loads the map image from a file.
     */
    private void loadMapImage() {
        try {
            mapImage = ImageIO.read(new File("shared/resources/MoonMap.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Filter and encoded the message to add the Frog previous and current position to the Frog Path array, as well as update its new angle. Then update the 2D map by repainting it.
     *
     * @param message message received from the ground control dashboard.
     */
    public void update2DMap(String message) {
        //Encode message:
        if (message.contains("Frog Path First Point: ")) {
            frogPath.clear();
            frogPath.add(new Point2D.Double(FROG_STARTING_POSITION_X, FROG_STARTING_POSITION_Z));
        } else if (message.contains("Frog Path: ")) {
            double frogX = Double.parseDouble(message.split(" ")[2]);
            double frogZ = Double.parseDouble(message.split(" ")[3]);
            Point2D.Double currentPosition = new Point2D.Double(frogX, frogZ);
            frogPath.add(currentPosition);
        } else if (message.contains("Frog Path Last Point: ")) {
            double frogX = Double.parseDouble(message.split(" ")[4]);
            double frogZ = Double.parseDouble(message.split(" ")[5]);
            Point2D.Double currentPosition = new Point2D.Double(frogX, frogZ);
            frogPath.add(currentPosition);
            AutomaticMovement.updateStartPos(frogPath.get(frogPath.size() - 1));
        } else {
            double newAngle = Double.parseDouble(message.split(" ")[2]);
            double nextAngle = newAngle - previousAngle;
            direction.rotateImg(nextAngle);
            previousAngle = newAngle;
            AutomaticMovement.updateStartAngle(previousAngle);

            //Re-draw
            repaint();
        }
    }

    /**
     * Convert the Frog actual coordinate (Position) in 3D map to the 2D map of the UI
     *
     * @param frogXRealPos x coordinate of the actual position on 3D map
     * @param frogZRealPos z coordinate of the actual position on 3D map
     * @return Since the map is rotated in the UI, the 3D Z coordinate is converted to 2D X coordinate while 3D X coordinate is converted to 2D Y coordinate
     */

    private Point2D.Double calculatePosition(double frogXRealPos, double frogZRealPos) {
        double xPosOnUI = (frogZRealPos - MAP_2D_BOTTOM_LEFT_Z) * (MAP_2D_UI_HEIGHT_GROUND_CONTROL) / (MAP_2D_TOP_RIGHT_Z - MAP_2D_BOTTOM_LEFT_Z); // x on UI is according to z on map
        double yPosOnUI = (frogXRealPos - MAP_2D_BOTTOM_LEFT_X) * (MAP_2D_UI_WIDTH_GROUND_CONTROL) / (MAP_2D_TOP_RIGHT_X - MAP_2D_BOTTOM_LEFT_X); // y on UI is according to x on map

        return new Point2D.Double(xPosOnUI, yPosOnUI);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        //Draw map
        double mapX = TILE_SIZE;
        double mapY = TILE_SIZE;
        g.drawImage(mapImage, (int) mapX, (int) mapY, MAP_2D_UI_WIDTH_GROUND_CONTROL, MAP_2D_UI_HEIGHT_GROUND_CONTROL, null);

        //Driven route
        g.setColor(Color.YELLOW);
        int pointSize = 4;
        for (Point2D.Double point : frogPath) {
            Point2D.Double previousPoint = calculatePosition(point.getX(), point.getY());
            double pointX = mapX + previousPoint.getX() - pointSize / 2;
            double pointY = mapY + previousPoint.getY() - pointSize / 2;
            g.fillOval((int) pointX, (int) pointY, pointSize, pointSize);
        }

        //Set up Frog position
        Point2D.Double frogPos = calculatePosition(frogPath.get(frogPath.size() - 1).getX(), frogPath.get(frogPath.size() - 1).getY());
        double width = 18;
        double height = 18;
        double x = mapX + frogPos.getX() - width / 2; // -27
        double y = mapY + frogPos.getY() - height / 2; // -66

        //Set up direction & Draw
        direction.draw(g, (int) x, (int) y, (int) width, (int) height);

        //Destination
        g.setColor(Color.green);
        g.fillOval((int) destination.x - 5, (int) destination.y - 5, 10, 10);

        //Radar Points
        g.setColor(Color.BLUE);

        for (nl.saxion.ptbc.ObjectDisplay.Point2D point : allRadarPoints) {
            Point2D.Double pointMap = calculatePosition(point.getX(), point.getZ());

            g.fillOval((int) ((int) mapX + (pointMap.getX() - 1)), (int) (mapY + pointMap.getY() - 1), 2, 2);
        }
    }

    /**
     * Sets the list of all radar points.
     *
     * @param allRadarPoints the list of radar points to set
     */
    public void setAllRadarPoints(ArrayList<nl.saxion.ptbc.ObjectDisplay.Point2D> allRadarPoints) {
        this.allRadarPoints = allRadarPoints;
    }

    public Rectangle2D.Double getClickZone() {
        return clickZone;
    }

    /**
     * Order the Frog to move to a destination
     *
     * @param des A point that has x and y representing the coordinate of the destination
     */
    public void moveAutomaticallyToPoint(Point2D.Double des) {
        Point2D.Double start = frogPath.get(frogPath.size() - 1);
        System.out.println(frogPath.get(frogPath.size() - 1).x + " " + frogPath.get(frogPath.size() - 1).y);
        new AutomaticMovement(this, start, des, previousAngle);
    }
}
